#!/usr/bin/env bash

function check_export_outdate(){
    local md_filepath=$1
    echo "checking exports of ${md_filepath}"
#    echo "$md_filepath"
    local html_filepath="${md_filepath%.*}.html"
    local pdf_filepath="${md_filepath%.*}.html"

    local md_file_last_modify=`stat -c %Y ${md_filepath}`
    if [[ -e ${html_filepath} ]]; then
        check_file_outdate ${md_filepath} ${html_filepath}
    fi
    if [[ -e ${pdf_filepath} ]]; then
        check_file_outdate ${md_filepath} ${pdf_filepath}
    fi
}

function check_file_outdate(){
    local md_filepath=$1
    local export_filepath=$2

    if [[ "${export_filepath}" -ot "${md_filepath}" ]]; then
        echo -e "\033[31m ${export_filepath} is out of date...\033[0m"
    fi
}

function traverse_dir(){
    local cur_filepath=$1
#    echo "here...${cur_filepath}..."
    for filename in `ls ${cur_filepath}`; do
        local sub_filepath=${cur_filepath}/${filename}
#        echo ${sub_filepath}
        if [[ -d ${sub_filepath} ]]; then
#            echo "${filename}"
            if [[ ${filename} != '.' && ${filename} != '..' ]]; then
                traverse_dir ${sub_filepath}
            fi
        elif [[ -f ${sub_filepath} ]]; then
            local file_extension="${filename##*.}"
            if [[ ${file_extension} == "md" ]]; then
                check_export_outdate ${sub_filepath}
            fi
        fi
    done
}

if [[ -z $1 ]]; then
    checking_folder='.'
else
    checking_folder=$1
fi

if [[ ! -e ${checking_folder} ]]; then
    echo -e "\033[31m error ==> ${checking_folder} is not exist...\033[0m"
    exit
elif [[ ! -d ${checking_folder} ]]; then
    echo -e "\033[31m error ==> ${checking_folder} is not a folder...\033[0m"
    exit
fi

traverse_dir "${checking_folder}"