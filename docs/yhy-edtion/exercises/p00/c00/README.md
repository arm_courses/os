# 第0章： 计算机系统概述

1. 如何理解计算机系统的层次结构？

>答：
>
>（:book:P1-2）计算机系统一般可以理解为由以下几个层次组成：
>
>+ 硬件系统
>+ 系统软件
>+ 支撑软件
>+ 应用软件
>+ 最终用户
>
>层次结构的特点： 将整体问题局部化，将一个大型复杂的系统分解成若干意向依赖的层次，由各层的正确性来保证整个系统的正确性。
>层次结构的优点： 采用层次结构，能使结构清晰，便于调试，有利于功能的增、删和改，正确性容易得到保证，也提高了系统的可维护性和可移植性。

2. 简述计算机硬件、软件、操作系统三者之间的关系

>答：
>
>软件一般分为系统软件、支撑软件和应用软件，操作系统是重要的系统软件。
>
>硬件是底层基础，系统软件在硬件的基础上为支撑软件和应用软件服务，应用软件在支撑软件、系统软件和硬件的支持下为最终用户提供服务。

3. 冯诺依曼计算机的特点是什么？

>答：
>
>+ 硬件由运算器、控制器、（主）存储器、输入设备和输出设备五大部件组成
>+ 指令和数据以同等地位存放于（主）存储器中，并可按地址寻访
>+ 指令和数据均可以用二进制代码表示
>+ 指令由操作码和地址码组成，操作码表示操作的性质，地址码表示操作数据在（主）存储器的地址
>+ 指令在（主）存储器按顺序存放，通常，指令按顺序执行，在特定情况下，可根据运算结果或特定条件改变执行顺序
>+ 硬件以运算器为中心，IO设备和（主）存储器的数据传送均通过运算器

4. 试画出计算机硬件组成框图

>答：
>
>如下图

```dot
digraph hw{
    label="实线：数据流，虚线：控制流";
    labelloc=t;

    bgcolor=transparent;
    node[shape="box"];
    rankdir="LR";
    splines=ortho;
    edge[minlen=3];

    input_device[label="输入设备"];
    output_device[label="输出设备"];
    main_memory[label="主存储器"];
    controller[label="控制器"];
    processing_unit[label="运算器"];

    {
        controller, main_memory, processing_unit;
        rank="same";
    }

    {
        edge[style="solid"];
        processing_unit -> main_memory[dir="both"];
        main_memory -> controller;
        input_device -> main_memory -> output_device;
    }
    
    {
        edge[dir="both",style="dashed"];
        processing_unit -> controller;
        input_device -> controller;
        output_device -> controller;
        controller -> main_memory[dir="forward"];
    }
}
```

5. 计算机中哪些部件可以存储信息？

>答：
>
>+ 主机内：
>   + `CPU`内： `register`
>   + `CPU`内或`CPU`外： `cache`
>   + `main memory`
>+ 主机外：
>   + `local external storage`
>   + `remote external storage`

6. 什么是I/O接口？其功能是什么？

>答：
>
> lO接口是主机与IO设备进行信息交换的纽带，主机通过IO接口与IO设备进行数据交换。
> 
> IO接口的功能一般有：
>
>+ 地址
>   + 地址译码
>   + 设备选择
>+ 数据
>   + 数据缓冲（速度匹配）
>   + 信号转换： 并行串行转换、A/D转换等
>+ 控制
>   + 控制状态： 查询和设置状态等
>   + 控制逻辑： 如时序、中断、DMA控制逻辑、设备操作等

7. 存储容量和内存容量有何区别？

>答：
>
>内存容量： 可被`CPU`直接访问而不需要通过输入输出设备的存储设备。内存用于存储运算时的指令和数据，广义上包括`register`、`cache`和泛义的内存（一般表示为`RAM`）
>存储容量： 非易失的存储，用于静态地存储数据（程序、文档等）的存储空间

8. CPU有哪些功能？CPU结构是怎样的？

>答：
>
>`CPU`的功能： 解释指令和处理数据、控制系统运行
>`CPU`的结构： 见 :book: P4 图0-6

9. 计算机硬件性能评价指标有哪些？

>答：
>
>+ 字长：指`CPU`单次可处理的二进制位数
>+ 主频：指`CPU`的时钟频率，单位用`MHz`或`GHz`表示
>+ 速度：指计算机每秒中所能执行的指令条数，一般用`MIPS(Million Instructions Per Second)`为单位
>+ 内存储器容量和速度
>+ 外存储器容量和速度
>+ 外设配置
