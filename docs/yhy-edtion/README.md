# 计算机操作系统（郁红英）

1. 📖 理论：[《计算机操作系统（第3版）》](http://www.tup.tsinghua.edu.cn/booksCenter/book_07794401.html)，郁红英等编著，清华大学出版社
1. 📖 实验：[《计算机操作系统实验指导（第3版）》](http://www.tup.tsinghua.edu.cn/booksCenter/book_07794301.html)，郁红英（主编），清华大学出版社
