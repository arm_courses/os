# 《操作系统》综合考核

[TOC]

## 考核要求

以3至5人为一组（最多不超过6人），拟定课题题目，运用课程所学知识，完成组内分工、设计实现、文档撰写，提交工程目录文件夹（由组长提交）和主题报告（每位均需提交，课题下的某个主题），其他要求如下：

1. 拟题要求：组长与老师沟通确定课题，组长负责组织小组成员完成项目
2. 功能要求：具有一定的功能，能够正常运行，并且能应用于实践、解决某个具体问题
3. 代码要求：要求 **_不少于300行有效代码语句（不含注释、空行、单独的分隔符）_**
4. 语言要求：可采用合适的编程语言
5. 文档要求：按要求编写项目文档
6. 任务要求：以小组为单位完成项目作品，严禁抄袭他人作品，抄袭者和被抄袭者均按0分处理
7. 提交要求：
    1. 由组长提交工程目录文件夹压缩包，文件名为`组长学号_组长姓名_课题名称.zip`，如`20180001_张三_进程调度算法的模拟实现.zip`
    2. 每位同学提交主题报告（不少于2000字），文件名为`学号_姓名_主题名称.zip`，如`20180001_张三_进程调度算法演进历程分析.zip`

## 工程目录文件夹

提交的工程目录文件夹应包含以下内容：

```shell {.line-numbers}
.
├── .assets              # 使用word文档时，无此目录
│   ├── diagram          # 设计图
│   └── image            # 运行截图
├── README.md            # 项目文档，README.md文件或README.docx文件
└── codes                # 源代码，前端源代码和后端源代码均放到此目录下
```

## 选题参考

1. 操作系统经典算法的模拟实现，假如，进程控制原语、进程调度算法、死锁处理算法、存储管理算法、文件管理算法、文件系统、文件访问矩阵（权限访问矩阵）等
2. `shell script`编程，假如，系统监控、系统管理等
3. 其他与`Linux`操作系统相关的开发、测试、运维项目（需利用`Linux`的特性，并运行在`Linux`上）

❗ 不少于300行有效代码语句（不含注释、空行、单独的分隔符），具体课题题目请组长与老师沟通确定 ❗

## 可供参考的参考文献

1. [cxuan. 5万字、97 张图总结操作系统核心知识点[EB/OL].](https://www.cnblogs.com/cxuanBlog/p/13297199.html)
2. [cxuan. 主宰操作系统的经典算法.](https://www.cnblogs.com/cxuanBlog/p/13372092.html)
3. [tapaswenipathak/Visualization-of-CPU-Scheduling-Algorithms.](https://github.com/tapaswenipathak/Visualization-of-CPU-Scheduling-Algorithms): Visualization of CPU Scheduling Algorithms.
4. [CPU Scheduler Application.](https://jimweller.com/jim-weller/jim/java_proc_sched/)
