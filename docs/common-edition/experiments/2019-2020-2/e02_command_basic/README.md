# 实验项目02：Linux的基本操作和基本命令

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [实验基本信息](#实验基本信息)
2. [实验前置条件](#实验前置条件)
3. [实验内容指引](#实验内容指引)
    1. [`CentOS`登录、登出与关机重启](#centos登录-登出与关机重启)
    2. [用户信息查询](#用户信息查询)
    3. [文件查看命令](#文件查看命令)
    4. [帮助文档查询命令](#帮助文档查询命令)
    5. [内核版本信息查询](#内核版本信息查询)
    6. [发行版版本信息查询](#发行版版本信息查询)
4. [FAQ](#faq)

<!-- /code_chunk_output -->

## 实验基本信息

1. 实验性质：综合性
1. 实验学时：6
1. 实验目的与要求：
    1. 了解`Linux`的启动、登录、登出、关机、重启方法
    1. 了解`Linux`的`Terminal`
    1. 掌握`Linux`的`Terminal`基本命令
1. 实验内容：
    1. `CentOS`登录、登出与关机重启
    1. 用户信息查询
    1. 文件查看命令
    1. 帮助文档查询命令
    1. 内核版本信息查询
    1. 发行版版本信息查询
1. 实验条件：
    1. 硬件环境：`PC`(`Personal Computer`)
    1. 软件环境：`Windows` + `VirtualBox` + `CentOS`

## 实验前置条件

`Linux`环境： 可以基于虚拟机环境下安装的`Linux`，也可基于`Windows 10 WSL`环境下安装的`Linux`，也可基于裸机环境下安装的`Linux`，也可基于云服务器环境下安装的`Linux`。

## 实验内容指引

以下指引中使用尖括号`<>`表示键盘按键，如：`<CTRL>`代表control键，`<j>`代表j键

:exclamation::exclamation::exclamation:本实验内容指引下每节分成三部分:exclamation::exclamation::exclamation:

1. **相关操作与命令讲解** ==> 实验涉及的操作与命令的讲解
1. **:microscope: 实验具体内容：** ==> 实验的具体步骤
1. **参考结果截图** ==> 实验的参考结果截图，**实验报告应包含类似的截图**

### `CentOS`登录、登出与关机重启

<!--    1. 键盘`<CTRL> + <ALT> + <F1~F7>`： 在直连`TTY`中进行切换，`Linux`支持7个直连`TTY`登录不同的账户（其中，`F1`至`F6`键对应6个`CLI TTY`，`F7`对应`GUI TTY`，使用`GUI TTY`时需安装了`GUI`）
**:notebook_with_decorative_cover:备注说明：** `Linux`属于"多账户多用户多任务"`OS`，`Windows`属于"多账户单用户多任务"`OS`（"远程登录"`Windows`时，其他用户将被锁屏而无法操作）
-->

1. **相关操作与命令讲解**
    1. `uptime`： 查询开机时长、登录用户等信息
    1. `logout`： 注销（登出）当前用户
    1. `poweroff`或`shutdown -h now`： 关机
    1. `reboot`或`shutdown -r now`： 重启
1. **:microscope: 实验具体内容：**
    1. 使用`root`账号登录
    1. 输入`uptime`命令查询
    1. 输入`logout`命令登出`root`用户
    1. 重新使用`root`账号登录
    1. 输入`reboot`或`shutdown -r now`命令重启
    1. 重新使用`root`账号登录
    1. 输入`poweroff`或`shutdown -h now`命令关机
    1. 重新使用`root`账号登录

![0101_login](./assets/images/0101_login.png)

![0102_uptime](./assets/images/0102_uptime.png)

![0103_logout](./assets/images/0103_logout.png)

![0104_reboot](./assets/images/0104_reboot.png)

![0105_poweroff](./assets/images/0105_poweroff.png)

### 用户信息查询

1. **相关操作与命令讲解**
    1. `who`： 查询已登录的用户
    1. `whoami`： 查询当前`tty`的登录用户
    1. `tty`： 查询当前`tty`名称，`tty(TeleTYpe)`是一种早期的终端设备（类似打字机），`POSIX`通常使用`tty`来简称各种类型的终端设备，`Linux`支持7个直连`TTY`登录不同的账户，键盘`<CTRL> + <ALT> + <F1~F7>`在直连`TTY`中进行切换（其中，`F1`至`F6`键对应6个`CLI TTY`，`F7`对应`GUI TTY`，使用`GUI TTY`时需安装了`GUI`）
1. **:microscope:实验具体内容：**
    1. 输入`who`命令查询已登录的用户
    1. 输入`whoami`命令查询当前`tty`的登录用户
    1. 输入`tty`命令查询当前`tty`名称

![0201_accout&user](./assets/images/0201_accout&user.png)

### 文件查看命令

1. **相关操作与命令讲解**
    1. `cat`： "cat"是"concatenate"的缩写，用于将文件内容直接输出到`stdout`，`cat`一次性输出文件全部内容，当文件内容超过一个屏幕时，前面的内容将被冲出本屏幕
    1. `more`： `more`是对`cat`的改进，不再一次性输出全部内容，而是输出完一个屏幕后，`<SPACE>（空格键）`后输出下一个屏幕内容，`<q>`退出（quit）
    1. `less`： "less"取自"less is more"，`less`使用`vi`中`normal`模式的键盘映射，即`<j>`向下翻一行、`<k>`向上翻一行、`<CTRL> + <f>`向下翻一屏（forward）、`<CTRL> + <b>`向上翻一屏（backward）、`<CTRL> + <d>`向下翻半屏（down）、`<CTRL> + <u>`向上翻半屏（up）、`<q>`退出（quit）
1. **:microscope:实验具体内容：**
    1. 输入`cat /proc/cpuinfo`查看文件内容
    1. 输入`clear`命令清屏
    1. 输入`cat /proc/vmstat`查看文件内容
    1. 输入`more /proc/vmstat`查看文件内容
    1. 输入`clear`命令清屏
    1. 输入`less /proc/vmstat`查看文件内容

![0301_cat_cpuinfo](./assets/images/0301_cat_cpuinfo.png)

![0302_cat_vmstat](./assets/images/0302_cat_vmstat.png)

![0303_more_vmstat](./assets/images/0303_more_vmstat.png)

![0304_less_vmstat](./assets/images/0304_less_vmstat.png)

### 帮助文档查询命令

1. **相关操作与命令讲解**
    1. `man`： "man"是"manual"的缩写，`man`使用`less`作为文档输出，翻屏方式与`less`一致
    1. `info`： "info"是"infomation"的缩写，`<SPACE>`向下翻屏，`<PageDown>`翻屏，`<PageUp>`向上翻屏（:warning:笔记本电脑可能没有`<PageDown>`和`<PageUp>`），`<q>`退出（quit）
1. **:microscope:实验具体内容：**
    1. 输入`man more`查看`more`命令的帮助
    1. 输入`man less`查看`less`命令的帮助

![0401_man_more](./assets/images/0401_man_more.png)

![0402_info_less](./assets/images/0402_info_less.png)

### 内核版本信息查询

1. **相关操作与命令讲解**
    1. `uname`："uname"是"unix name"的缩写
    1. `cat /proc/version`： `/proc`目录是内核的内存映像，该目录下的文件存放于内存中的，它以文件系统的方式为访问系统内核数据的程序提供接口。`uname -a`获取的信息是从`/proc/version`文件获取
1. **:microscope:实验具体内容：**
    1. 输入`uname -a`查看内核信息
    1. 输入`cat /proc/version`查看内核信息

![0501_kernel_version](./assets/images/0501_kernel_version.png)

### 发行版版本信息查询

1. **相关操作与命令讲解**
    1. `cat /etc/os-release`： 详细的发行版版本信息
    1. `cat /etc/redhat-release`： `redhat distro`专有文件
    1. `cat /etc/issue`： `/etc/issue`在`CentOS`上基本没有有用信息
1. **:microscope:实验具体内容：**
    1. 输入`cat /etc/os-release`查看详细信息
    1. 输入`cat /etc/redhat-release`查看`redhat distro`专有文件信息
    1. 输入`cat /etc/issue`

![0502_distro_verison](./assets/images/0502_distro_verison.png)

## FAQ

请参见[FAQ常见问题集](../OS-Expt_FAQ/OS-expt_faq.md)
