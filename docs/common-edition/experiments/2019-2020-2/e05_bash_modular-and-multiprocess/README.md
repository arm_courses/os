# OS-E05 实验项目05： Bash模块化编程与多进程并发

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [实验基本信息](#实验基本信息)
2. [实验前置条件](#实验前置条件)
3. [实验预备知识](#实验预备知识)
4. [实验注意与说明](#实验注意与说明)
5. [实验内容与实验指引](#实验内容与实验指引)
    1. [:microscope: 实验内容具体任务](#microscope-实验内容具体任务)
    2. [:ticket: 实验结果参考截图](#ticket-实验结果参考截图)
    3. [:bookmark: 实验内容相关知识](#bookmark-实验内容相关知识)
        1. [Bash模块化](#bash模块化)
        2. [Bash多进程](#bash多进程)
        3. [Bash命令运行与算术运算](#bash命令运行与算术运算)
6. [FAQ](#faq)

<!-- /code_chunk_output -->

## 实验基本信息

1. 实验性质：设计性
1. 实验学时：6
1. 实验目的与要求：
    1. 理解模块化编程的思想与优势
    1. 掌握`bash`模块化编程的方法
    1. 理解多进程/多线程编程的优势和适用场景
    1. 掌握`bash`多进程编程的方法
1. 实验内容：
    1. 利用`function`和`source`编写多文件脚本，分别实现串行创建文件和并发创建文件，并对比运行时间
    1. 对比思考使用`sleep 0.1`与不使用`sleep 0.1`的运行结果产生差别的原因是什么
    1. 对比思考并发在什么场景下效率能显著提升，什么场景下效率并不能显著提升
1. 实验条件：
    1. 硬件环境：`PC`(`Personal Computer`)
    1. 软件环境：`VirtualBox` + `CentOS`

## 实验前置条件

`Linux`环境： 可以基于虚拟机环境下安装的`Linux`，也可基于`Windows 10 WSL`环境下安装的`Linux`，也可基于裸机环境下安装的`Linux`，也可基于云服务器环境下安装的`Linux`。

## 实验预备知识

请 **至少** 阅读[阮一峰 Bash 脚本教程](https://wangdoc.com/bash/)中的以下内容：

1. 循环： <https://wangdoc.com/bash/loop.html>
1. 算术运算： <https://wangdoc.com/bash/arithmetic.html>
1. 函数： <https://wangdoc.com/bash/function.html>

## 实验注意与说明

1. 实验指引中使用尖括号`<>`表示键盘按键，如：`<CTRL>`代表control键，`<j>`代表j键
1. 实验中请将`EN_and_FN_in_PY`改成您的学号和全名的拼音，如“68zhangsan”
1. 实验内容与实验指引中各子内容包含三部分：
    1. **:microscope: 实验内容具体任务** ==> 实验的具体任务及步骤
    1. **:ticket: 实验结果参考截图** ==> 实验的参考结果截图
    1. **:bookmark: 实验内容相关知识** ==> 实验涉及的相关知识

## 实验内容与实验指引

### :microscope: 实验内容具体任务

利用`function`和`source`编写多文件脚本，分别实现串行创建文件和并发创建文件，并对比运行时间，具体要求如下：

1. 程序的模块结构和功能
    1. `main.sh`： 该脚本文件有一个函数`main()`， **`main()`是脚本程序的入口函数，实现程序的业务逻辑** ，`main()`接受两个参数，第1个参数表示创建文件的目录，第2个参数表示创建文件的数量
    1. `seq_create_files.sh`： 该脚本文件有一个函数`f_seq_create_files()`， **该函数实现在指定的目录下串行创建指定数量的文件** ，该函数接受两个参数，第1个参数表示创建文件的所在目录，第2个参数表示创建文件的数量
    1. `con_create_files.sh`： 该脚本文件有一个函数`f_con_create_files()`， **该函数实现在指定的目录下实现并发创建指定数量的文件** ，该函数接受两个参数，第1个参数表示创建文件的所在目录，第2个参数表示创建文件的数量
    1. `create_file.sh`： 该脚本文件有一个函数`f_create_file()`，**该函数实现具体创建文件** ，该函数接受一个参数，该参数表示创建文件的路径（含文件名）
1. 程序的运行逻辑（业务逻辑）
    1. `main.sh`脚本文件中调用`main()`函数，传递`./<EN_and_FN_in_PY>`和`100`给`main()`函数（请将`EN_and_FN_in_PY`改成您的学号和全名的拼音，如“68zhangsan”）
    1. `main()`函数中，在第1参数传递的目录路径下分别创建`seq`和`con`目录，分别传递给`f_seq_create_files()`和`f_con_create_files()`，将`100`也传递给`f_seq_create_files()`和`f_con_create_files()`
    1. `f_create_file()`中为了体现串行和并发的差别，在该函数中调用`sleep 0.1`
1. 请注释掉`f_create_file()`中的`sleep 0.1`，并将创建文件的数量调高至`1000`，再次运行，然后对比思考：
    1. 对比思考使用`sleep 0.1`与不使用`sleep 0.1`的运行结果产生差别的原因是什么
    1. 对比思考并发在什么场景下效率能显著提升，什么场景下效率并不能显著提升（有时甚至带来反效果）

![overall_seq](./assets_diagram/overall_seq.svg)

### :ticket: 实验结果参考截图

![100_sleep](./assets_image/100_sleep.png)

![1000_nosleep](./assets_image/1000_nosleep.png)

### :bookmark: 实验内容相关知识

#### Bash模块化

1. `function`： `function`是实现`code reuse`和`modular programming`的基础，与`command`或`shell script`不同的是，`function`在当前`shell`环境中运行（脚本可通过`source`在当前`shell`环境下运行），调用`function`的语法与`shell script`和`command`一样，因此，为了显式区分`function`，建议在`function name`之前加上`f_`或其它便于识别的前缀
1. `function script`： 将`function`组织在`function script`文件中，有利于进一步`code reuse`和`modular programming`
1. `source`命令或`.`命令（点命令）： 在当前`shell`环境下运行`script`（而不是新建一个`shell`进程），被运行`script`的`variable`和`function`在当前`shell`中有效（不使用`source`运行`script`时为子`shell`中运行，被运行`script`的`variable`和`function`在当前`shell`中无效），`source`在`${PATH}`环境变量路径上寻找`script`或`command`

#### Bash多进程

1. `&`： 后台运行，当前`shell`不必阻塞等待运行结果而直接运行下一条命令
1. `wait`命令： 等待所有子进程运行完毕
1. `sleep`命令： 进程休眠指定秒数

#### Bash命令运行与算术运算

1. `$()`表达式： 运行命令并返回运行结果
1. `$(())`表达式： 进行整型算术运算并返回运算结果

## FAQ
