---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Synchronization and Mutual Exclusion_"
footer: "@aRoming"
math: katex

---

<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Synchronization and Mutual Exclusion

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [What is Synchronization and What is Mutual Exclusion](#what-is-synchronization-and-what-is-mutual-exclusion)
2. [同步机制](#同步机制)
3. [经典同步问题](#经典同步问题)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## What is Synchronization and What is Mutual Exclusion

---

1. **mutual exclusion**: **_间接制约关系_**，临界资源一次只能被一个进程访问
    - processes unaware of each other进程不知道对方的存在
    - competition竞争
    - `mutual exclusion`, `deadlock`, `starvation`
2. **synchronization**: **_直接制约关系_**，多个进程为了合作完成任务，必须严格按照规定的 **某种先后次序** 来运行
    - processes indirectly aware of each other进程间接地知道对方的存在
    - cooperation by sharing通过共享进行协作
    - `mutual exclusion`, `deadlock`, `starvation`, `data coherence`

---

1. **communication**: **_紧密协作关系_**，多个进程为了合作完成任务，必须通过 **某种通信机制** 来进行信息交换
    - process directly aware of each other进程直接地知道对方的存在
    - cooperation by communication通过通信进行协作
    - `synchronization`, `deadlock`, `starvation`

$\text{communication} \supset \text{synchronization} \supset \text{mutual exclusion}$

---

1. **mutual exclusion** vs **synchronization**: 同步中包含了/实现了互斥，同步是更为复杂的互斥，互斥是特殊的同步
    - 互斥是指某一资源同时只允许一个访问者对其进行访问，具有唯一性和排它性。互斥不会限制访问者对资源的访问顺序，即访问是无序的
    - 同步是指在互斥的基础上（大多数情况），通过其它机制实现访问者对资源的有序访问
2. **synchronization** vs **communication** : 同步是一种特殊的/间接的通信

---

1. 同步：使并发执行的诸进程之间能有效地共享资源和相互合作，从而使程序的执行具有确定性和可重现性
2. 异步 --> 相互独立，同步 --> 相互依赖
3. 进程间的制约关系
    1. 直接制约关系：同步关系，进程间按一定的次序相互协作
    2. 间接制约关系：互斥关系，进程间相互竞争使用临界资源
4. 进程的同步往往伴随着互斥

<!--TODO:
"往往"还是"肯定"？
-->

---

### 临界资源与临界区

1. 临界资源：一次只允许一个进程使用的资源，进程间应互斥地对这种资源的进行共享
2. 临界区：涉及临界资源互斥访问的代码段
    1. 进入区/请求：用于检查是否可以进入临界区的代码段
    2. 临界区：互斥访问临界资源
    3. 退出区/归还：将临界区正被访问的标志恢复为未被访问标志的代码段
    4. 剩余区：非互斥代码段

---

```cpp {.line-numbers}
...
进入区;
临界区;
退出区;

剩余区;
...
```

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 同步机制

---

### 同步机制的基本原则

1. 空闲让进：当无进程处于临界区，应允许请求进入临界区的进程进入临界区
2. 忙则等待：当有进程处于临界区，其它请求进入临界区的进程必须等待
3. 有限等待：等待进入临界区的进程不能一直处于等待状态
4. 让权等待：不能进入临界区的进程，应释放`CPU`并转换到`blocked`

---

### 同步机制的实现方法

1. 软件同步机制：使用编程方法解决临界区问题，有一定难度、具有局限性，目前较少采用
2. 硬件同步机制：使用特殊的硬件指令
3. 信号量机制：程序员使用`PV`/`wait()-signal()`原语编程实现
4. 管程机制：由系统提供的一种同步互斥服务

<!--TODO:

已知：

1. 管程机制是信号量机制的一种扩展
2. 信号量机制是硬件同步机制的一种抽象？

待解：

同步机制之间的核心区别？从哪些维度进行比较？

1. 硬件同步机制 VS 软件同步机制
2. 信号量抽制 VS (硬件同步机制 and 软件同步机制)
3. 信号量机制 VS 管程机制

-->

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 硬件同步机制

---

1. 对临界区进行管理时，可以将标志当成一个锁，"锁开"进入，"锁关"等待，初始时锁是打开的
2. 每个进程在进入临界区之前，先对锁进行测试

---

#### 关中断实现方式

1. 关中断：在进入临界区之前，关闭中断，直至完成锁测试并上锁后，才恢复中断
2. 优点：能保证互斥
3. 缺点：
    1. 滥用关中断权力可能会导致严重后果
    2. 关中断时间过长会影响系统效率
    3. 不能在`multi-processor`系统中使用

---

#### `Test-and-Set`指令实现方式

1. `Test-and-Set`指令：原语操作
2. 为每个临界资源设置一个`bool`类型的`lock`，初始值为`false`，表示未被占用
3. 循环测试，不符合"让权等待"原则

---

```cpp {.line-numbers}
bool TestAndSet(bool *lock) {
    bool old = *lock;
    *lock = true;
    return old;
}
```

```cpp {.line-numbers}
do{
    while TestAndSet(&lock) ; // busy waiting
    // critical section
    lock = false;
    // remainder section
}
```

---

#### `swap`指令实现方式

1. 为每个临界资源设置一个全局的`bool`类型的`lock`，初始值为`false`，表示未被占用
2. 在每个进程中设置一个局部的`bool`类型的`key`，初始值为`true`
3. 使用`swap`指令与对`lock`和`key`进行交交，以此循环判断`lock`的值
4. 只有当`key`为`false`时，才可进行临界区操作
5. 循环测试，不符合"让权等待"原则

---

```cpp {.line-numbers}
do{
    key = true;
    do{
        swap(&key, &lock);
    }while(key != false);   // busy waiting
    // critical section
    swap(&key, &lock);
    // remainder section
}
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 信号量机制

---

1. 1965年，由荷兰学者`E.W.Dijkstra`提出
2. `P`、`V`分别代表荷兰语的`Proberen (attempt)`和`Verhogen (increment)`
3. `wait()-signal()`是`P(s)-V(s)`操作的另一种描述形式
4. 信号量类型
    1. 整型信号量
    2. 记录型信号量
    3. AND型信号量
    4. 信号量集

<!--TODO:

信号量机制为什么能做"让权等待"的同时，又能"原子性"？

-->

---

#### 整型信号量

```cpp {.line-numbers}
typedef int semaphore;
wait(s){
    while(s <= 0) ; // busy waiting
    s--;
}

signal(s){
    s++;
}
```

1. 初始指定一个正整数值，表示空闲资源总数（又称为"资源信号量"）
2. 正值时表示当前的空闲资源数，
3. 当`s <= 0`时，未遵循"让权等待"原则

<!--TODO: 有可能是负值吗？

若为负值其绝对值表示当前等待临界区的进程数？

-->

---

#### 记录型信号量

1. 每个信号量`s`除一个整数值`s.value`外，还有一个队列`s.list`，存放阻塞在该信号量的各个进程`pcb`

---

```cpp {.line-numbers}
typedef struct {
    int value;
    struct pcb_list *list;
}semaphore;
```

```cpp {.line-numbers}
wait(semaphore *s){
    s->value--;
    if(s->value < 0) block(s->list);
}

signal(semapore *s){
    s->value++;
    if(s->value <= 0) wakeup(s->list);
}
```

---

#### AND型信号量

1. 将进程在某一时刻需要的多种资源（每种1个），在一个原语中分配给进程，使用完后在一个原语中释放
2. 对若干个临界资源的分配，采用原语
3. 可以避免死锁

---

```cpp {.line-numbers}
swait(S1，S2，…，Sn)  {
    while (true)  {
        if (Si>=1 && … && Sn>=1) {
            for (i =1;i<=n;i++) Si--;；
            break;
        }else {
            //place the process in the waiting queue associated
            //with the first Si found with Si<1，
            //and set the program count of this process
            //to the beginning of Swait operation
            for(i =1; i<=n && )
        }
    }
}
```

---

```cpp {.line-numbers}
ssignal(S1，S2，…，Sn)  {
    while (true) {
        for (i=1 ; i<=n;i++) {
            Si++；
            //Remove all the process waiting in the queue
            //associated with Si into the ready queue.
        }
    }
}
```

---

#### 信号量集

1. `wait(s)`和`swait(s)`每次仅对某类临界资源进行一个单位的申请
2. `swait(s, t, d)`，其中`t`代表申请资源时的该资源的数量下限（即当`s->value >= t`时才分配资源），`d`代表申请资源的实际数量
3. `ssignal(s, d)`，释放`d`个资源

---

1. `swait(s,d,d)`: 申请`d`个资源，若`d`个资源都可用，则分配，否则等待
2. `swait(s,1,1)`: 申请一个资源，若资源可用则分配，否则等待，蜕化为一般的记录型信号量（`s > 1`时）或互斥信号量（`s == 1`时）
3. `swait(s,1,0)`: 试探资源，可控开关

<!--TODO:
通用的写法是`swait`?
-->

---

#### 利用信号量实现互斥

```cpp {.line-numbers}
semaphore mutex = 1;

pa(){
    wait(mutex);
    // critical section
    signal(mutex);
    // remainder section
}
```

---

#### 利用信号量实现同步

```cpp {.line-numbers}
semaphore s = 0;

p1(){
    c1();
    signal(s);
    // remainder section
}

p2(){
    wait(s);
    c2();
    // remainder section
}
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 管程机制

---

#### 信号量机制的缺点与解决方法

1. 缺点：
    1. 需要程序员编码实现
    2. 维护困难、容易出错
        1. `wait`和`signal`位置错
        2. `wait`和`signal`不配对
2. 解决方法：由编程语言或公共工具解决同步互斥问题
3. 信号量是分散式，管程是集中式

---

1. 一个管程定义了一个数据结构和能为并发进程所执行（在该数据结构上）的一组操作，这组操作能同步进程和改变管程中的数据

![height:400](./.assets/image/monitor.jpg)

---

1. 互斥：
    1. 管程中的变量只能被管程中的操作访问
    2. 任何时候只有一个进程在管程中操作，类似临界区
1. 同步：
    1. 条件变量
    2. `block`和`wakeup()`

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 经典同步问题

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### Dining Philosophers Problem哲学家进餐问题

---

1. 五个哲学家的生活方式：交替地进行"思考"和"进餐"两个活动
1. 五个哲学家共用一张圆桌，分别坐在五张椅子上，圆桌上有五个碗和五支筷子
2. 哲学家饥饿时试图拿起左右两支筷子，当且仅当拿到两支筷子时才能进餐，进餐完毕后，放下筷子继续思考

---

![height:600](./.assets/image/dining_philosophers_problem.png)

---

### 记录型信号量实现哲学家进餐问题

```cpp {.line-numbers}
semaphore chopstick[5] = {1, 1, 1, 1, 1};

philosopher i:

while(true){
    think();
    wait(chopstick[i]);
        wait(chopstick[(i + 1) % 5]);
            eat();
        signal(chopstick[(i + 1) % 5]);
    signal(chopstick[i]);
}
```

---

1. 可能引起死锁，如，五个哲学家同时饥饿而各自拿起左筷子时，会使信号量`chopstick`均为0
2. 解决方法
    1. 最多允许4个哲学家同时坐在桌子周围（必定有一个哲学家可以拿到两支筷子）
    2. 奇数号的哲学家必须首先拿左边的筷子，偶数号的哲学家则反之
    3. 仅当一个哲学家左右两边的筷子都可用时，才允许他拿筷子

---

### AND信号量实现哲学家进餐问题

```cpp {.line-numbers}
semaphore chopstick[5] = {1, 1, 1, 1, 1};

philosopher(int i){
    while(true){
        think();
        swait(chopstick[i], chopstick[(i + 1) % 5]);
            eat();
        ssignal(chopstick[i], chopstick[(i + 1) % 5]);
    }
}
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 生产者-消费者问题

---

1. 生产者-消费者问题是相互合作进程关系的一种抽象
1. 多个生产者进程负责生产数据，多个消费者进程负责消费数据
2. 缓冲区的容量为N，生产者和消费者共享缓冲区
3. 缓冲区需互斥访问

---

![width:1120](./.assets/image/producer-consumer.png)

---

1. 需要协同的部分
    1. 生产者：把产品放入指定缓冲区
    2. 消费者：从缓冲区取出一个产品
2. 三种情况
    1. 缓冲区空，消费者等待缓冲区不为空
    2. 缓冲区满，生产者等待缓冲区不为满
    3. 缓冲区既不空也不满，生产者和消费者都可以操作

---

#### 记录型信号量实现生产者-消费者问题

```cpp {.line-numbers}
int in = 0, out = 0;
product buffer[N];
semaphore mutex = 1, empty = N, full = 0;

void producer(){
    product item;
    while(true){
        produce_item(&item);
        wait(empty);
            wait(mutex);
                buffer[in] = item;
                in = (in + 1) % N;
            signal(mutex);
        signal(full);
    }
}
```

---

```cpp {.line-numbers}
void consumer(){
    product item;
    while(true){
        wait(full);
            wait(mutex);
                item = buffer[out];
                out = (out + 1) % N;
            signal(mutex);
        signal(empty);
        consume_item(&item);
    }
}
```

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 读者-写者问题

---

1. 读者-写者问题是指多个进程对共享数据进行读写操作的问题
2. 有两类并发进程，共享一组数据区：多个读者进程和多个写者进程
3. 公共约束/数据区约束：读写互斥、写写互斥、读读不互斥
    1. 允许多个读者同时读操作
    1. 不允许读者、写者同时操作
    1. 不允许多个写者同时写操作
1. 场景约束：
    1. 读者优先
    2. 写者优先
    3. 先到优先

---

#### 场景约束--等待区

1. 读者优先
    1. 到达进程：读 --- 写 --- 读
    2. 执行次序：读 --> 读 --> 写
2. 先到优先
    1. 到达进程：写 --- 读 --- 写
    2. 执行次序：写 --> 读 --> 写
3. 写者优先
    1. 到达进程：读 --- 写 --- 写
    2. 执行次序：写 --> 写 --> 读

---

#### 读者优先

1. 如果读者申请进入数据区
    1. 无读者，无写者 ==> 新读者进入数据区
    2. 有读者，无写者 ==> 新读者进入数据区
    3. 无读者，有写者 ==> 新读者进入等待区
2. 如果写者申请进入数据区
    1. 无读者，无写者 ==> 新写者进入数据区
    2. 有读者，无写者 ==> 新写者进入等待区
    3. 无读者，有写者 ==> 新写者进入等待区

<!--
---

1. 读者等待区：有读者等待时，写者退出数据区后，读者进入数据区
2. 写者等待区：有写者等待时，第一个等待的写者等待读者等待区为空时，进入数据区 -->

---

```cpp {.line-numbers}
int reading_count = 0;                  // reader waiting number
semaphore reading_count_mutex = 1;      // mutex exclusion for read_count
semaphore data_mutex = 1;               // mutex exclusion for reader && writer

void reader(){
    while(true){
        wait(reading_count_mutex);
            if(reading_count == 0) wait(data_mutex);
            reading_count++;
        signal(reading_mutex);

        read();

        wait(reading_count_mutex);
            reading_count--;
            if(reading_count == 0) signal(data_mutex);
        signal(reading_count_mutex);
    }
}

void writer(){
    while(true){
        wait(data_mutex);
            write();
        signal(data_mutex);
    }
}
```

---

#### 先到优先

1. 如果读者申请进入数据区
    1. 无读者，无写者 ==> 新读者进入数据区
    2. 有读者 ==> 新读者进入数据区
    3. 无读者，有写者 ==> 新读者进入等待区
2. 如果写者申请进入数据区
    1. 无读者，无写者 ==> 新写者进入数据区
    2. 有读者，无写者 ==> 新写者进入等待区
    3. 无读者，有写者 ==> 新写者进入等待区

---

```cpp {.line-numbers}
int reading_count = 0;                  // reader waiting number
semaphore reading_count_mutex = 1;      // mutex exclusion for read_count
semaphore data_mutex = 1;               // mutex exclusion for reader && writer
semaphore writer_waiting_mutex = 1;     // reader wait at least one writer

void reader() {
    while (true) {
        wait(writer_waiting_mutex);
            wait(reading_count_mutex);
                if (reading_count == 0) wait(data_mutex);
                reading_count ++;
            signal(reading_count_mutex);
        signal(writer_waiting_mutex);

        read();

        wait(reading_count_mutex);
            reading_count--;
            if (reading_count == 0) signal(data_mutex);
        signal(reading_count_mutex);
    }
}

void writer() {
    while (true) {
        wait(writer_waiting_mutex);
            wait(data_mutex);
                write();
            signal(data_mutex);
        signal(writer_waiting_mutex);
    }
}
```

---

#### 写者优先

1. 如果读者申请进入数据区
    1. 无读者，无写者 ==> 新读者进入数据区
    2. 有读者，无写者等待 ==> 新读者进入数据区
    3. 无读者，有写者 ==> 新读者进入等待区
    4. 有写者待 ==> 新读者进入等待区
2. 如果写者申请进入数据区
    1. 无读者，无写者 ==> 新写者进入数据区
    2. 有读者，无写者 ==> 新写者进入写者等待区
    3. 无读者，有写者 ==> 新写者进入写者等待区
    4. 有写者 ==> 新写者进入写者等待区

---

```cpp {.line-numbers}
int reading_count = 0;              // current reading number
int writer_waiting_count = 0;       // writer waiting number
semaphore reading_count_mutex = 1;  // mutex exclusion for reading_count
semaphore writer_waiting_count_mutex = 1;              // mutex exclusion for write_count
semaphore data_mutex = 1,           // mutex exclusion between reader and writer
semaphore writer_waiting_mutex = 1; // reader wait at least one writer

void reader() {
    while (true) {
        wait(writer_waiting_mutex);
            wait(reading_count_mutex);
                if (reading_count == 0) wait(data_mutex);
                reading_count ++;
            signal(reading_count_mutex);
        signal(writer_waiting_mutex);

        read();

        wait(reading_count_mutex);
            reading_count --;
            if (reading_count == 0) signal(data_mutex);
        signal(reading_count_mutex);
    }
}

void writer() {
    while (true) {
        wait(writer_waiting_count_mutex);
            if (writer_count == 0) wait(writer_waiting_mutex);
            write_count ++;
        signal(writer_waiting_count_mutex);

        wait(data_mutex);
            write();
        signal(data_mutex);

        wait(writer_waiting_count_mutex);
            write_count --;
            if (writer_count == 0) signal(writer_waiting_mutex);
        signal(writer_waiting_count_mutex);
    }
}
```

<!--
https://zhuanlan.zhihu.com/p/538487720
-->

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
