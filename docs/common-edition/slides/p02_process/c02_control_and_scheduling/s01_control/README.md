---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Process Control_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Process Control

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [进程控制](#进程控制)
2. [进程状态与进程控制原语](#进程状态与进程控制原语)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 进程控制

---

1. 进程控制：对某个进程进行控制，包括进程状态的控制及其转换、进程资源的分配和回收等
2. 狭义的进程控制指的是进程状态的控制及其转换
3. 进程控制是进程管理最基本的功能
4. 一般由`os kernel`中的原语实现

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 进程状态与进程控制原语

---

### 进程的2个状态及其转换

1. `running执行态/运行态`：已获得CPU，处于执行的状态
2. `non-running非执行态`：未获得CPU，处于非执行的状态

---

### 进程的3个状态及其转换

1. `ready就绪态`：已获得除CPU外的所有资源，等待CPU的资源
2. `blocked阻塞态`：因等待某事件发生而暂时无法继续执行，可以根据阻塞原因设置多个阻塞队列

---

![width:1120](./.assets/image/process-3-states.jpeg)

---

#### `switch()`切换

1. `process switch`
    1. as known as `context switch`
    2. `context switch` is the `process` of storing the state of a `process` or `thread`

---

2. when to switch a `process`
    1. `scheduling`
    2. `other`
        1. `interrupts`
            - `clock interrupt`
            - `IO interrupt`
        2. `memory fault`
        3. `trap`
            - `system call`: such as `file` `open()`

---

##### `switch()`过程

1. handle current `process`
    - save execution context
    - update `pcb`
    - move `pcb` to appropriate queue
2. handle another process
    - select another process for next execution from `ready queue`
    - update `pcb`
    - restore execution context

---

#### `block()`阻塞

1. 进程阻塞的原因
    1. 向系统请求共享资源失败
    2. 等待某种操作的完成
    3. 新数据尚未到达
    4. 等待新任务的到达
2. 进程的阻塞是进程自身发起的行为的结果

---

##### `block()`过程

1. 停止执行
2. 进程状态由`running`改成`blocked`，`switch()`
3. 插入阻塞队列

---

#### `wakeup()`唤醒

1. 进程唤醒的原因：阻塞进程所等待的事件得到满足

---

##### `wakeup()`过程

1. 从`blocked queue`中移出
2. 进程状态由`blocked`改为`ready`
3. 将`pcb`插入`ready queue`

---

### 进程的5个状态及其转换

---

1. `create创建态`/`new新建态`：进程正在被创建，申请一个新的`pcb`并填写相关信息、分配资源，然后设置为`ready`
2. `terminated终止态`/`exit退出态`：进程执行完毕或者被强制终止，等待善后、回收`pcb`

---

![width:1120](./.assets/image/process-5-states.jpeg)

---

#### `create()`创建

1. 进程具有层次结构，进程树是用于描述进程家族关系的有向树，`pstree`
2. 触发进程创建的事件：
    1. 初始化进程创建子进程
    2. 用户登录
    3. 父进程创建子进程

---

##### `create()`过程

1. 申请`pcb`空间
2. 分配所需资源
3. 初始化`pcb`
4. 插入`ready queue`

---

#### `terminate()`终止

1. 进程终止的原因
    1. 正常终止
    2. 异常终止
    3. 被其他进程终止

---

##### `terminate()`过程

1. 根据被终止进程的标识符，从PCB集合中检索出该进程的PCB，从中读出该进程的状态
2. 若被终止进程正处于执行状态，应立即终止该进程的执行，并设置调度标志为真，用于指示该进程被终止后应重新进行调度
3. 若该进程还有子孙进程，还应将其所有子进程予以终止
4. 将该进程所拥有的全部资源，或者归还给其父进程或系统
5. 将被终止进程（PCB）从所在队列中移去

---

### 进程的7个状态及其转换

---

1. `ready-suspended就绪挂起态/静止就绪态`：进程处于就绪态，但由于某种原因被挂起
2. `blocked-suspended阻塞挂起态/静止阻塞态`：进程处于阻塞态，但由于某种原因被挂起
3. 原来的`ready`称为`就绪态`或`活动就绪态`，原来的`blocked`称为`阻塞态`或`活动阻塞态`

---

![height:600](./.assets/image/process-7-states.jpeg)

---

#### `suspend()`挂起

1. 进程挂起的原因
    1. 用户的需要：用户指定挂起某些进程，例如：`debug`
    2. 父进程的需要：考查或修改子进程或协调各子进程
    3. `OS`的需要：检查或记录进程资源使用情况
    4. 资源不足
        1. 内存不足：挂起一些进程，释放内存
        2. `I/O`不足：挂起一些进程，释放`I/O`设备
2. 进程挂起是进程外部原因产生的结果

---

##### `suspend()`过程

1. 检查将被挂起进程的状态，将其状态改为相应状态，并将其插入相应队列
    1. 若为`running`或`ready`，则将其状态改为`ready-suspended`
    2. 若为`blocked`，则将其状态改为`blocked-suspended`
2. 若被挂起的进程原为`running`，则转向调度程序进行调度
3. 为方便用户或父进程检查该进程的运行情况，将其`pcb`复制到某个指定的内存区域

---

#### `active()`激活

1. 进程激活的原因：发生引发进程激活的事件

---

##### `active()`过程

1. 检查将被激活进程的状态，将其状态改为相应状态，并将其插入相应队列
    1. 若为`ready-suspended`，则将其状态改为`ready`
    2. 若为`blocked-suspended`，则将其状态改为`blocked`
2. 如果系统采用的是抢占调度策略，则每当有进程添加到`ready queue`时，应发起进程调度

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
