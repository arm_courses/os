---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Introduction to Operating System_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Introduction to Operating System操作系统概述

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Goals目标](#goals目标)
2. [操作系统的目标和作用](#操作系统的目标和作用)
3. [操作系统的发展历程](#操作系统的发展历程)
4. [操作系统的基本特征](#操作系统的基本特征)
5. [操作系统的硬件环境](#操作系统的硬件环境)
6. [操作系统的核心功能](#操作系统的核心功能)
7. [操作系统的结构体系](#操作系统的结构体系)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Goals目标

---

### 知识目标

1. 了解操作系统的基本概念
2. 了解操作系统的发展历程

---

### 能力目标

1. 能辨别具体操作系统的类型和功能
2. 能选择适合应用场景的操作系统

---

### 素养目标

1. 养成良好的科学分析意识、结构思维意识、问题提出意识
1. 养成良好的工匠精神、团队合作意识、创新意识

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 操作系统的目标和作用

---

![height:600](./.assets/image/httpatomoreillycomsourcemspimages1792257.png)

---

![height:600](./.assets/image/httpatomoreillycomsourcemspimages1792259.png)

---

### 操作系统的目标（商业需求/业务需求）

1. 方便性：通过操作系统，用户可以方便地使用计算机
2. 有效性：
    1. 提高计算机系统的资源利用率
    2. 提高计算机系统的吞吐量
3. 可扩展性：操作系统应该具有良好的可扩展性，能方便地添加新的功能和模块，对原有的功能和模块进行修改
4. 开放性：操作系统应该具有良好的开放性，能方便地与其他系统进行通信和交换信息

---

### 操作系统的作用（用户需求）

1. 计算机系统资源的管理者（操作系统作为资源管理者）：进程管理、内存管理、设备管理、文件管理、接口管理
2. 计算机系统资源的虚拟化（操作系统作为虚拟扩展机）：提供一个对硬件资源的虚拟视图，使用户得到一个更强大、更易用的计算机系统

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 操作系统的发展历程

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 1st generation(1945-1955): `No Operating System`

---

1. `vacuum tubes真空管`
2. `no operating system`
3. `single programming`
4. `io mode`
    1. `manual io`
    2. `offline io`

---

#### `Manual IO`人工IO操作方式

1. 人工操作计算机IO
2. 特点
    1. 用户独占计算机
    2. CPU等待人工操作

---

![height:600](./.assets/image/ENIAC-1946.jpg)

---

>ENIAC, **the first programmable general-purpose** electronic digital computer
>
>ENIAC was enormous. It occupied the **50-by-30-foot (15-by-9-metre)** basement of the Moore School, where its 40 panels were arranged, U-shaped, along three walls.
>
>Completed by February 1946, ENIAC had cost the government **$400,000**, and the war it was designed to help win was over.
>
>>Swaine, M. R. and Freiberger, . Paul A. (2024, February 20). ENIAC. Encyclopedia Britannica. <https://www.britannica.com/technology/ENIAC>

---

#### `Offline IO`脱机IO操作方式

1. 输入输出在外围机控制下完成，主机和外围机通过磁带或其他存储设备传输数据
2. 优点
    1. 减少CPU等待时间
    2. 提高IO效率

---

![height:500](./.assets/image/punched_tape.jpg)

>[punched tape](https://www.edaboard.com/media/punched-tape-same-as-with-punch-cards-punched-tape-was-originally-pioneered-by-the-textile-industry-for-use-with-mechanized-looms-for-computers-p.57050/)

---

![height:440](./.assets/image/Keypunch_operator_1950_census_IBM_016.jpg)

>clerk creating punch cards in 1950.
>>[Punched Card.](https://en.wikipedia.org/wiki/Punched_card)

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 2nd Generation(1955-1965): `Batch Operating System批处理操作系统`

---

1. `transistors晶体管`
2. `batch operating systems`/`batch monitor`
    1. `simple batch operating system`/`simple batch monitor`
    2. `multiprogramming batch operating system`/`multiprogramming batch monitor`

---

#### `Simple Batch Operating System单道批处理操作系统`

1. 处理过程：作业一个接一个地连续处理
2. 缺点：内存中只有一道作业，后续作业需等待前继作业完成，作业IO操作时CPU空闲

---

![height:520](./.assets/image/ibm1401-1959.jpeg)

---

#### `Multi-Programming Batch Operating System多道批处理操作系统`

1. 处理过程：内存中同时存放多道作业，CPU在多道作业之间切换执行（作业在IO操作时主动将CPU资源释放）
2. 缺点
    1. 无人机交互功能
    2. 作业周转时间无法有效控制

---

![height:600](./.assets/image/simple-batch_and_multiprogramming-batch.jpeg)

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 3rd Generation(1961-1980): `Time-Sharing Operating System分时操作系统`

---

1. `IC(Integrated Circuit, 集成电路)`
2. `time-sharing operating system`/`multi-user operating system`
3. `multi-tasking`

---

1. 用户需求：人机交互、共享主机
2. 共享主机
    1. multi-tasking by a single user
    2. multiple user sessions
3. 关键问题：如何使用户能与自己的作业进行交互
    1. 及时接收：多路卡、缓冲区
    2. 及时响应：多道程序设计、时间片轮转

---

#### 分时操作系统的特点

1. 多路性：允许多台终端同时连接到同一台主机，并分时使用
2. 独立性：用户感觉独占主机
3. 及时性：用户请求能在短时间内获得响应
4. 交互性：用户能与自己的作业进行交互

---

#### 典型分时操作系统

1. 1961：`CTSS(Compatible Time-Sharing System)`
2. 1965: `Multics(Multiplexed Information and Computing Service)`
3. 1969: `Unics(Uniplexed Information and Computing Service)` --> `UNIX`

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 4st generation(1971-present): `Real Time Operating System实时操作系统` and Others

---

1. `VLSI(Very Large Scale Integration, 超大规模集成电路)` && `pc, network and cluster`
2. 类型
    1. `real-time operating system`
    2. `network operating system`, `personal operating system`/`microcomputer operating system`/`desktop operating system` , `embedded operating system`
    3. `general operating system`
3. `multi-tasking` && `distributed computing`

---

#### `Real Time Operating System实时操作系统`

1. 用户需求：时效性（系统的正确性不仅需要结果正确，同时要求规定时间内完成处理）
2. 系统能及时响应外部事件的请求，在规定的时间内完成对该事件的处理，并控制所有实时任务协调地运行
3. 特点：实时性

---

#### 实时操作系统的类型

1. 按任务执行时是否呈现周期性划分
    1. 周期性实时系统
    2. 非周期性实时系统
2. 按对截止时间的时限要求划分
    1. `Hard Real Time`硬实时系统
    2. `Soft Real Time`软实时系统

---

#### 实时操作系统的场景

1. 工业（武器）控制系统
2. 交通控制系统
3. 金融交易系统
4. 电话通信系统

---

#### 其他维度划分类型的操作系统

1. `network operating system网络操作系统`：具备网络通信功能的操作系统，目前已不再单独列明`网络操作系统`
2. `embedded operating system嵌入式操作系统`：应用于嵌入式系统的操作系统
3. `personal operating system个人操作系统`：应用于个人计算机的操作系统（一般指`x86`架构的个人计算机），目前基本与`microcomputer operating system微机操作系统`/`desktop operating system桌面操作系统`同义
4. `general purpose operating system`通用操作系统：具有多种类型操作特征的操作系统，可以同时兼有批处理、分时、实时功能，或其中两种以上的功能的操作系统

---

#### `Distributed Operating System分布式操作系统`与`Cluster Operating System集群操作系统`

1. 运行在多台计算机上的操作系统，将多台计算机逻辑上组成一个单一的系统
2. 分布式操作系统构建在（单物理机）操作系统之上，目前，分布式操作系统主要是集群操作系统
3. 集群的组件通常通过快速局域网相互连接，每个节点（用作服务器的计算机）运行自己的操作系统实例
4. `Hadoop`，`Spark`, `Kubernetes`等
5. 特点：分布性、透明性、同一性、全局性

---

### 推动操作系统发展的主要因素

1. 对操作系统需求的不断追求
    1. 业务需求
    2. 用户需求
    3. 功能需求与非功能需求
2. 计算机硬件技术的不断发展
    1. 计算机体系结构的不断发展
    2. 计算机器件的不断更新换代

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 操作系统的基本特征

---

1. `concurrence并发`：多个程序同时在计算机上运行
2. `sharing共享`：多个程序共享计算机系统的资源
3. `virtual虚拟`：通过操作系统，用户得到一个对硬件资源的虚拟视图
4. `asynchronous异步`/`indeterminacy不确定性`：多个程序的执行是异步的，程序的执行速度是不确定的

---

### Concurrence并发 VS Parallelism并行

1. `concurrence并发`：微观上串行、宏观上并行，两个或两个以上的事件在 **_同一时间间隔内_** 发生
2. `parallelism并行`：微观上并行、宏观上并行，两个或两个以上的事件在 **_同一时刻_** 发生

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 操作系统的硬件环境

---

### 计算机启动过程

1. `BIOS`：计算机加电后，首先执行`BIOS`程序
2. `MBR`：`BIOS`程序读取硬盘的第一个扇区，即`MBR`（Master Boot Record）扇区
3. `Boot Loader`：`MBR`扇区中包含`Boot Loader`程序，`Boot Loader`程序加载操作系统内核
4. `Kernel`：操作系统内核加载到内存中，开始运行
5. `Init`：操作系统内核初始化，启动`init`进程
6. `User Space`：`init`进程启动其他用户进程
7. `Login`：用户登录系统，启动`shell`或`gui`进程

---

### 硬件支撑环境

1. `interruption中断`：用于处理当前指令本身功能之外的事件的机制
2. `clock时钟`：时钟信号，用于同步与协调各部件执行的基础机制

<!--
1. `hardware interruption`/`external interruption`/`interruption`：，由硬件引起的中断
2. `software interruption`/`internal interruption`/`exception`：由软件引起的中断
    1. `exception handling`：被迫中断，地址越界、算术溢出、除零、缺页等
    2. `trap`：自愿中断，`system call`, `debugging`等
-->

---

1. `primitive原语`/`atomic`：用于完成一定功能的若干条指令组成的指令集合，这些指令集合要么全做、要么都不做，形成不可分割的原子操作
2. `protection rings指令环`：用于区分不同的特权指令集，一般至少有两个特权级别（`kernel mode内核态`和`user mode用户态`）

---

![height:600](./.assets/image/protection_rings.jpeg)

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 操作系统的核心功能

---

1. 硬件资源管理
    1. 进程管理：`CPU`资源
    2. 内存管理：`memory`资源
    3. 设备管理：`io device`资源
2. 软件资源管理
    1. 文件管理
    2. 接口管理
        1. 人机接口
        2. 程序接口

---

### 进程管理

1. 进程控制：进程状态控制
2. 进程调度：`CPU`资源分配
3. 进程同步：进程间执行顺序的协调
4. 进程通信：进程间信息的交换

---

### 内存管理

1. 内存空间分配：内存资源分配与回收
2. 内存地址映射：逻辑地址与物理地址的映射
3. 内存空间保护：进程间内存空间的隔离
4. 内存空间扩充：逻辑上扩充内存空间

---

### 设备管理

1. 设备分配：`io device`资源分配
2. 设备驱动：`abstract io device`与`real io device`的映射
3. 设备通信：`CPU`与`io device`的信息交换

---

### 文件管理

1. 文件存储：文件的存储与组织
2. 文件操作：文件的打开、读写和关闭
3. 文件保护：文件的访问权限控制

---

### 接口管理

1. 人机接口：用户与计算机的交互
    1. 联机用户接口/命令行用户接口
    2. 脱机用户接口
    3. 图形用户接口
2. 程序接口：其他程序与操作系统的交互，`system call系统调用`

---

![width:1120](./.assets/image/system-call.png)

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 操作系统的结构体系

---

### Modularization Perspective模块化视角

1. `layered structural style`分层结构风格：层间通信，上层只依赖于直接下层的服务
2. `hierarchical structural style`层级结构风格：层间通信，上层依赖于直接或间接下层的服务
3. `modular structural style`分块结构风格：块间通信，块间可相互依赖

---

### Privileged Perspective特权视角

1. `single-mode structural style`单模态结构风格
2. `dual-mode structural style`双模态结构风格：`kernel mode` and `user mode`
3. `multi-mode structural style`多模态结构风格

---

![height:600](./.assets/image/protection_rings.jpeg)

---

### Kernel Process Perspective内核进程视角

1. `monolithic kernel`宏内核/单体内核: entire `kernel` as a single `process` run in `kernel space`, eg, most of `UNIX`, `Linux`
    1. **single process, single address**
    2. **invoke directly**
2. `micro kernel`微内核: small `kernel core` that contains only essential `OS` functions run in `kernel space`, eg, `macOS`, `Windows NT`
    1. **multi processes, multi address**
    2. **client-server, IPC based**

---

### Essentials of `Micro Kernel`

1. `process management`: `process control`, `process scheduling`, `process synchronization` ,`process communication`
2. `memory management`: `address translate`
3. `interrupt management`

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
