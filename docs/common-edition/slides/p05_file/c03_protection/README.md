---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_File Protection_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# File Protection

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [文件安全与文件保护](#文件安全与文件保护)
2. [访问矩阵](#访问矩阵)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 文件安全与文件保护

---

### 影响文件安全性的主要因素与对应措施

1. 人为因素，采用存取控制机制应对
2. 系统因素，采用容错技术应对
3. 自然因素，采用后备系统应对

---

### 访问权与保护域

1. 访问权：一个进程能对某对象执行操作的权利
2. 访问权的表示方法：(对象名，权集)，如`(F1, {RW})`
3. 保护域：进程对一组对象访问权的集合，称为保护域
    1. 如果进程和域一一对应，则称为静态联系方式，该域称为静态域
    2. 如果进程和域是一对多关系，则称为动态联系方式

![height:160](./.assets/image/image-20240428075619434.jpeg)

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 访问矩阵

1. 用矩阵来描述系统的访问控制
2. 行代表域，列代表对象，每一元素代表访问权

![height:200](./.assets/image/image-20240428075821855.png)

---

### 具有域切换权的访问控制矩阵

1. 能将进程从一个保护域切换为另一个域
2. 例如，由于域D1和D2所对应的项目中有一个`S`（switch，即切换权），则允许在域D1中的进程切换到域D2中

![height:200](./.assets/image/image-20240428075950993.png)

---

### 访问矩阵的修改

1. 复制权：将某个域所拥有的访问权access（i，j）扩展到同一列的其他域中
2. 所有权：能增加某种访问权，或者能删除某种访问权
3. 控制权：用于改变矩阵内同一行（域）中的各项访问权，亦即，用于改变在某个域中运行的进程对不同对象的访问权

---

![height:200](./.assets/image/image-20240428081036940.png)

---

![height:200](./.assets/image/image-20240428081044915.png)

1. 图中的"O"表示所有权
2. 图（b）所示为：在域D1中运行的进程删除了在域D3中运行的进程对文件F1的执行权；在域D2中运行的进程增加了在域D3中运行的进程对文件F2和F3的写访问权

---

![height:200](./.assets/image/image-20240428081105783.png)

1. 若在access（D2，D3）中包括了控制权，则一个在域D2中运行的进程能够改变对域D3的各项访问权

---

### 访问矩阵的实现

1. 稀疏矩阵
2. 访问控制表`ACL(Access Control List)`：
    1. 对访问矩阵按列(对象)划分，为每一列建立一张访问控制表ACL，删除空项，由(域，权集)组成
    2. 当对象是文件时，ACL存放在文件的文件控制表中
3. 访问权限表
    1. 对访问矩阵按行(域)划分，为每一行建立一张访问权限表
    2. 当对象是文件时，访问权限表用来描述一个用户对文件的一组操作

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
