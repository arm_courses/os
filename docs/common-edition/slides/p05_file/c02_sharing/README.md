---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_File Sharing_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# File Sharing

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [基于索引节点的文件共享](#基于索引节点的文件共享)
2. [基于符号链接的文件共享](#基于符号链接的文件共享)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 基于索引节点的文件共享

---

1. 设置链接计数`count`

![height:400](./.assets/image/image-20240426222449101.jpeg)

---

![height:400](./.assets/image/image-20240426222536546.jpeg)

<!-- ---

#### 无环图目录

1. 有共享的子目录和文件

![height:500](./.assets/image/image-20240426221331499.jpeg) -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 基于符号链接的文件共享

---

1. 符号链接：一个文件的内容是另一个文件的路径
2. 优点：不会出现在文件拥有者删除一个共享文件后留下一个悬空指针的情况
3. 缺点：
    1. 符号链接指向的文件被删除后，符号链接仍然存在
    2. 每次访问符号链接都要经过两次外存访问

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
