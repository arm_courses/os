# Introduction to Operating System操作系统概述

- [1. Agenda](#1-agenda)
    - [1.1. Main Topics](#11-main-topics)
    - [1.2. Keys for Programming](#12-keys-for-programming)
    - [Terminologies](#terminologies)
- [2. What is Operating System操作系统是什么](#2-what-is-operating-system操作系统是什么)
- [3. Requirements of OS操作系统的需求分析](#3-requirements-of-os操作系统的需求分析)
    - [3.1. `OS`的业务需求](#31-os的业务需求)
    - [3.2. `OS`的用户需求](#32-os的用户需求)
    - [3.3. `OS`的质量需求](#33-os的质量需求)
- [4. Types of OS操作系统的类型](#4-types-of-os操作系统的类型)
    - [4.1. 按任务特征分类（基本分类）](#41-按任务特征分类基本分类)
    - [4.2. 按并发度分类](#42-按并发度分类)
    - [4.3. 按硬件场景分类](#43-按硬件场景分类)
- [5. Basic Concepts of OS操作系统的基础概念](#5-basic-concepts-of-os操作系统的基础概念)
    - [5.1. Job, Process/Task and Thread作业，进程/任务和线程](#51-job-processtask-and-thread作业进程任务和线程)
    - [5.2. Concurrency and Parallel并发与并行](#52-concurrency-and-parallel并发与并行)
    - [5.3. Memory and Storage内存与外存](#53-memory-and-storage内存与外存)
    - [5.4. Virtual Memory , File and Virtual File System虚拟内存，文件与虚拟文件系统](#54-virtual-memory--file-and-virtual-file-system虚拟内存文件与虚拟文件系统)
    - [5.5. Offline IO, Online IO and SPOOLing IO](#55-offline-io-online-io-and-spooling-io)
- [6. The Architecture of OS操作系统架构](#6-the-architecture-of-os操作系统架构)
    - [6.1. Modularization Perspective模块化视角](#61-modularization-perspective模块化视角)
    - [6.2. Privileged Perspective特权视角](#62-privileged-perspective特权视角)
    - [6.3. Kernel Process Perspective内核进程视角](#63-kernel-process-perspective内核进程视角)
- [7. POSIX and UNIX](#7-posix-and-unix)
    - [7.2. POSIX&reg;, SUS&reg;/UNIX&reg; and UNIX-like](#72-posix-susunix-and-unix-like)
    - [7.3. macOS](#73-macos)
    - [7.4. Linux](#74-linux)
        - [7.4.1. Is Linux a POSIX? Is Linux a SUS/UNIX?](#741-is-linux-a-posix-is-linux-a-susunix)
        - [Linux Kernel Architecture](#linux-kernel-architecture)
- [8. OS Timelines操作系统时间线](#8-os-timelines操作系统时间线)
    - [8.1. UNIX-like Timeline](#81-unix-like-timeline)
        - [8.1.1. UNIX and UNIX-like Timeline](#811-unix-and-unix-like-timeline)
        - [8.1.2. Linux Distribution Timeline](#812-linux-distribution-timeline)
    - [8.2. Non-UNIX-like Timeline](#82-non-unix-like-timeline)
        - [8.2.1. Windows Timeline](#821-windows-timeline)

## 1. Agenda

### 1.1. Main Topics

1. what is `OS`: the definition, functions, types, architectures of `OS`
2. `OS` in theory and in real world
3. main `OS` in real world
4. `OS`, `OSS` and `FOSS`
5. `OS` timelines

### 1.2. Keys for Programming

1. terms that we should know and use in communication with other persons
2. distinguish and choose what `OS` we should based on in our requirement

### Terminologies

1. **`OS(Operating System)`**
2. **`OSS(Open-Source Software)`**
3. **`FOSS(Free and Open-Source Software)`** :

## 2. What is Operating System操作系统是什么

1. an operating system (`OS`) is **system software** that **manages** computer hardware, software resources, and **provides** common services for computer programs.
1. short history of `OS` generation
    1. 1st generation(1945-1955): `Vacuum Tubes`, `no operating system`/`single programming`
        1. manual io
        2. offline io
    2. 2nd generation(1955-1965): `Transistors and Batch Systems`
        1. `simple batch operating system`/`monitor`
        2. `multiprogramming batch operating system`
    3. 3rd generation(1965-1980): `ICs and Multiprogramming`
        1. `time-sharing operating system`/`multi-user operating system`
    4. 4st generation(1980-present): `PCs, Networks and Clusters`
        1. `real-time operating system`
        2. `network operating system`, `microcomputer operating system` , `embedded operating system`
        3. `general system`
2. 什么样的`OS`是`Modern OS`？/`Modern OS`的特征是什么？
    1. `concurrency并发`（两个基本特征之一，最重要、最基本的特征）
    2. `sharing共享`（两个基本特征之一）
    3. `virtual虚拟`
    4. `asynchronous异步`/`indeterminacy不确定性`
3. `Traditional OS`发展成`Modern OS`的原因和动力是什么？
    1. 硬件的发展
        1. 指令集的发展
        2. 性能的发展
        3. 通信的发展： 机内通信、IO通信（外设通信、网络通信）
        4. 内存的发展
        5. 外存的发展
    2. 应用的需要：应用的需要同时会促进硬件和`OS`的发展
        1. 更有效地使用
        2. 更多的新应用
    3. 安全的需要：
        1. `4A(Authentication, Authorization, Account, Audit)`
4. `OS`对硬件的依赖
    1. `timer`定时器
    2. `IO interrupt`IO中断
    3. `DMA` or `channel`
    4. `privileged instructions`特权指令
    5. `MPM(Memory Protection Mechanism)`内存保护机制/`MMU(Memory Management Unit)`内存管理单元: micro-processor from `Intel 80386`
5. `OS`是什么？
    1. 两种观点
        1. 资源管理的观点：软硬件的资源管理器，负责分配、监控、回收软硬件资源
        2. 用户的观点/扩展机器的观点/虚拟机的观点：在祼机的物质基础上叠加一层软件，从而使原来的祼机更强大、更易用
    2. 四种观点
        1. 从外部看`OS`
            1. 计算机用户的观点：用户环境观点
            2. 应用程序员的观点：虚拟机器观点
        2. 从内部看`OS`
            1. `OS`开发者观点之一：资源管理观点
            2. `OS`开发者观点之二：任务组织观点

## 3. Requirements of OS操作系统的需求分析

### 3.1. `OS`的业务需求

1. 提高资源的效率
2. 方便用户的使用

### 3.2. `OS`的用户需求

1. `process management`进程管理/`processor management`处理机管理: 管理`CPU`资源
2. `memory management`内存管理: 管理内存资源
3. `device management`设备管理: 管理IO资源
    1. `disk management`磁盘管理: 文件的硬件载体，物理容器
4. `file management`文件管理: 管理文件资源
    1. `file system`文件系统: 文件的软件容器，逻辑容器
5. `interface`接口：
    1. `human-machine interface`人机交互界面/`end-user interface`最终用户界面: maybe in, also maybe not in `OS kernel`
        1. `online end-user command interface`联机用户命令界面
            - `CLI(Command Line Interface)`: eg, `shell`, `Windows CMD`, `Windows PowerShell`
            - `TWIN(Textmode WINdow)`: eg, `top`, `nano`, `vim`, `emacs`
        2. `offline end-user command interface`脱机用户命令界面
            - `JCL(Job Control Language)`/`batch processing`
        3. `online graphical user interface`联机图形用户界面
            - `GUI(Graphical User Interface)`: eg, `X Window System`, `Windows Explorer`
    2. `program interface`程序接口/`system call`系统调用: in `OS kernel`
        1. `POSIX(Portable Operating System Interface)`, `LSB(Linux Standards Base)`
        2. `WinAPI`: `Win16`, `Win32`, `Win32s`, `Win64`, `WinCE`

### 3.3. `OS`的质量需求

1. 外部质量属性：
    1. performance/efficiency（性能/效率）：
        1. throughput（吞吐量）
        2. response time（响应时间）
        3. processing time（处理时间）
        4. turnaround time（周转时间）
        5. processor utilization（处理机利用率）
    2. fairness（公平性）
    3. reliability（可靠性）
    4. security（安全性）
2. 内部质量属性：
    1. scalability（可伸缩性）
    2. extensibility（可扩展性）
    3. portability（可移植性）

## 4. Types of OS操作系统的类型

### 4.1. 按任务特征分类（基本分类）

1. `Batch Processing System批处理系统`：强调系统的资源利用率和吞吐量
    1. `Simple Batch Processing System单道批处理系统`：内存中只有一个任务，后续任务需等待前继任务完成
    2. `Multi-programming Batch Processing System多道批处理操作系统`：内存中同时有多个任务，当前执行任务在IO操作时释放CPU资源
2. `Time Sharing System分时操作系统`：强调人机交互、响应时间和资源均衡分配
3. `Real Time System实时操作系统`：强调紧密任务的处理时效性

### 4.2. 按并发度分类

1. **single user, single tasking system** 单用户单任务操作系统 ： `DOS(Disk Operating System)`
1. **single user, multi tasking system** 单用户多任务操作系统 ： `Windows`，`Windows Server`经修改配置后支持多用户，`Windows 10`通过特殊操作后可支持多用户(⚠️ **unconfirmed** )，`Window 10以下`版本未经实验证实是否支持多用户
1. **multi user, multi tasking system** 多用户多任务操作系统 ： `UNIX-like`

### 4.3. 按硬件场景分类

1. **microcomputer OS**: `macOS`, `GUN/Linux`, `Windows`, `DOS`
2. **multi-processor or multi-core OS**: `UNIX-like`, `Windows`(`Windows Vista` and later)
    - `SMP`(Symmetric MultiProcessing, 对称多处理器): processors are connected to a single, shared main memory, have full access to all input and output devices, and are controlled by a single operating system instance that treats all processors equally(defined by wiki)
3. **network OS**: `UNIX-like`, `Windows`
4. **embedded OS**:  `iOS`, `iPadOS`, `watchOS`, `tvOS`,`Android`
5. **distributed OS**:  `Hadoop`, `Kubernetes`

## 5. Basic Concepts of OS操作系统的基础概念

### 5.1. Job, Process/Task and Thread作业，进程/任务和线程

1. `job`: user perspective/application perspective, one `job` contain one or more `process`
1. `process` and `thread`: `OS` perspective
    - `process`: resource allocate unit, in `Linux` also name it `task`
    - `thread`: schedule unit(`kernel thread`)

### 5.2. Concurrency and Parallel并发与并行

1. **`concurrency`** : processing one process in micro-time, but as do some jobs at the same time in macro-view
2. **`parallel`** : processing more than process in the same time, require more than one processors

### 5.3. Memory and Storage内存与外存

1. `memory`/`main memory`/`primary memory`内存/主存：内存储器
2. `storage`/`secondary storage`/`secondary memory`外存/辅存：外存储器

### 5.4. Virtual Memory , File and Virtual File System虚拟内存，文件与虚拟文件系统

1. **`virtual memory`** : each process action as owns the whole memory address, need translate from `logical address` to `physical address`
2. **`file`** : abstract of a IO device(in `UNIX-like`)
3. **`virtual File System`: abstract the file system, provide a unified interface(`system call`) for different file systems

### 5.5. Offline IO, Online IO and SPOOLing IO

1. `offline IO`
2. `online IO`
3. `SPOOLing IO(Simultaneous Peripheral Operations On-Line)`外部设备联机并行操作（假脱机技术）
    - **purpose:** usually used for mediating between computer and slow peripheral, such as a printer
    - **key technologies:**
        1. virtual: one peripheral virtual to many logical peripheral
        2. queue

## 6. The Architecture of OS操作系统架构

### 6.1. Modularization Perspective模块化视角

1. `layered structural style`分层结构风格：层间通信，只与上下两层通信
2. `modular structural style`分块结构风格：块间通信
<!-- 2. `hierarchical structural style`层级结构风格 -->

<!--TODO:是否存在层级结构？-->

### 6.2. Privileged Perspective特权视角

1. `single-Mode structural style`单模态结构风格
2. `dual-Mode structural style`双模态结构风格：`user mode` and `kernel mode`
3. `multi-mode structural style`多模态结构风格

### 6.3. Kernel Process Perspective内核进程视角

1. `monolithic kernel`宏内核/单体内核: entire `kernel` as a single `process` run in `kernel space`, eg, most of `UNIX`, `Linux`
    1. **single process, single address**: **a single large process** running entirely in **a single address space**, all kernel services exist and execute **in the kernel address space**, and a single static binary file
    2. **invoke directly**: the kernel can invoke functions directly.
2. `micro kernel`微内核: small `kernel core` that contains only essential `OS` functions run in `kernel space`, eg, `macOS`, `Windows NT`
    1. **multi processes, multi address**: the kernel is broken down into separate processes, known as servers. Some of the servers run in kernel space and some run in user-space, all servers are kept separate and run in different address spaces
    2. **client-server, IPC based**: servers invoke "services" from each other by sending messages via `IPC (Inter Process Communication)`

![Monolithic Kernel vs. Microkernel](./.assets/image/monolithic_kernel_vs_microkernel.png)
>[Antonio Giacomelli de Oliveira. Monolithic Kernel and Microkernel.](https://antoniogiacomelli.com/2022/12/12/blog-monolithic-kernel-and-microkernel/)

>1. [Antonio Giacomelli de Oliveira. Monolithic Kernel and Microkernel.](https://antoniogiacomelli.com/2022/12/12/blog-monolithic-kernel-and-microkernel/)
>2. [StackOverflow. What is difference between monolithic and micro kernel?](https://stackoverflow.com/a/5194239)

## 7. POSIX and UNIX

### 7.2. POSIX<sup>&reg;</sup>, SUS<sup>&reg;</sup>/UNIX<sup>&reg;</sup> and UNIX-like

1. **`POSIX`(Portable Operating Systems Interface) certification**
    + is a family of standards specified by the IEEE Computer Society for maintaining compatibility between operating systems. Started at 1988
    + defines rules for a standard behavior and interface, such as:
        + standard C operations
        + multi-tasking
        + error states
        + command line && commands
    + standard enforced by IEEE && maintained by Austin Group (IEEE Computer Society, The Open Group, ISO/IEC JTC 1)
        + <http://get.posixcertified.ieee.org/certification_guide.html>
        + implements a set of automated conformance tests
        + need fee to certified( **at least \$2,000/annum** ), <http://get.posixcertified.ieee.org/docs/posix-fee-schedule-1.3.PDF>
1. **`SUS`(Single UNIX Specification)/`UNIX` certification**
    + `UNIX` is a trademark and brand, being `UNIX` certified(testing for compliance with `SUS`) makes the `OS` being a `UNIX`
    + developed and maintained by the Austin Group
    + significant licence fee( **\$1,000/annum + many many others** ), <https://www.opengroup.org/openbrand/Brandfees.htm>
    + UNIX<sup>&reg;</sup> Certified Products, <https://www.opengroup.org/openbrand/register/> and <https://www.opengroup.org/openbrand/register/index2.html>
1. `UNIX-like`: behaves in a manner similar to a UNIX system, sometimes referred to as `UN*X` or `*nix`

>Back in the day, there were many differences between `SUS` and `POSIX`, but today `SUS` is just `POSIX + Curses`.(`Curses` is terminal control library for `UNIX-like` systems)
>>[Greg's Wiki. POSIX.](https://mywiki.wooledge.org/POSIX)

### 7.3. macOS

>[iOS，macOS，darwin，unix这四个系统之间有什么区别与联系？](https://www.zhihu.com/question/40563950)

![MacOS_Architecture](./.assets/image/MacOS_Architecture.svg)

![Diagram_of_Mac_OS_X_architecture](./.assets/image/Diagram_of_Mac_OS_X_architecture.svg)

>[wiki. Architecture of macOS.](https://en.wikipedia.org/wiki/Architecture_of_macOS)

### 7.4. Linux

#### 7.4.1. Is Linux a POSIX? Is Linux a SUS/UNIX?

**From a business perspective, `Linux` is not `POSIX` and not a `SUS`/`UNIX`** , for these reasons:

1. `Linux` is a kernel, and `POSIX` does not specify a kernel interface
2. (⚠️ **unconfirmed** )`POSIX` certification need fee, so `Linux` is just **mostly `POSIX-compliant`**
3. (⚠️ **unconfirmed** )`SUS` certification need more fee, except below `Linux` distros:
    - [`Huawei EulerOS`](https://developer.huaweicloud.com/ict/cn/site-euleros/euleros)
    - [`Inspur K-UX`](https://en.inspur.com/en/2402170/2402191/2407691/index.html), **certification has expired**

**From a engineering perspective, `Linux` is `POSIX` and a reborn `UNIX`**

1. `Linux` philosophy is based on `UNIX` philosophy and extends it in certain areas
2. `Linux` is based on `LSB` which is based on `POSIX`,`SUS`, and several other open standards, and extends them in certain areas.

>1. [sirredbeard/Awesome-UNIX](https://github.com/sirredbeard/Awesome-UNIX#unix-certified-linux-based-operating-systems)
>2. [Linux Standards](https://www.linux.org/threads/linux-standards.11759/)
>3. [Posix Standard](https://linuxhint.com/posix-standard/)

#### Linux Kernel Architecture

![linux kernel architecture](./.assets/image/linux_kernel_architecture.png)
>[The Linux Kernel - Introduction.](https://linux-kernel-labs.github.io/refs/heads/master/lectures/intro.html)

![One architectural perspective of the Linux kernel](./.assets/image/major_subsystems_of_the_linux_kernel.jpg)

>[IBM Developer. Anatomy of the Linux kernel.](https://developer.ibm.com/articles/l-linux-kernel/)

## 8. OS Timelines操作系统时间线

>1. [Éric Lévénez's site.](https://www.levenez.com/)
>2. [UNIX发展史(BSD,GNU,linux).](https://blog.51cto.com/yjplxq/1034705)

### 8.1. UNIX-like Timeline

#### 8.1.1. UNIX and UNIX-like Timeline

![unix_generations](./.assets/image/unix_generations.jpg)

#### 8.1.2. Linux Distribution Timeline

![gldt1202](./.assets/image/gldt1202.svg)

### 8.2. Non-UNIX-like Timeline

<http://shannon.usu.edu.ru/History/oshistory/>

#### 8.2.1. Windows Timeline

<https://www.levenez.com/windows/>
