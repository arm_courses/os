# Part 03: Memory Management内存管理

- [1. Agenda](#1-agenda)
    - [1.1. Main Topics](#11-main-topics)
    - [1.2. Keys for Programming](#12-keys-for-programming)
- [2. Address Space地址空间](#2-address-space地址空间)
- [3. Programming Phases编程的阶段](#3-programming-phases编程的阶段)
- [4. Simple Memory Management基本内存管理](#4-simple-memory-management基本内存管理)
    - [4.1. Allocate Types分配类型](#41-allocate-types分配类型)
    - [4.2. Address Translation地址转换](#42-address-translation地址转换)
- [5. Memory Hierarchy存储器的层次结构](#5-memory-hierarchy存储器的层次结构)
    - [5.1. Memory Hierarchy in Type](#51-memory-hierarchy-in-type)
    - [5.2. Memory Hierarchy in Real World](#52-memory-hierarchy-in-real-world)
        - [5.2.1. Memory Performance in a Nutshell](#521-memory-performance-in-a-nutshell)
        - [5.2.2. Computer Time in Human Terms](#522-computer-time-in-human-terms)
        - [5.2.3. Latency Comparison Numbers (~2012)](#523-latency-comparison-numbers-2012)
- [6. Virtual Memory虚拟内存](#6-virtual-memory虚拟内存)
    - [6.1. VM in SR Perspective](#61-vm-in-sr-perspective)
    - [6.2. Reconsider Medium-Term Scheduling and Swap再议"中程调度与交换"](#62-reconsider-medium-term-scheduling-and-swap再议中程调度与交换)
    - [6.3. Resident Management驻留集管理](#63-resident-management驻留集管理)
    - [6.4. Fetch Policy调入策略](#64-fetch-policy调入策略)
    - [6.5. Replacement Algorithm置换算法](#65-replacement-algorithm置换算法)
    - [6.6. Cleaning Policy/Post-Replacement Policy清理策略](#66-cleaning-policypost-replacement-policy清理策略)
    - [6.7. Load Control Policy负载控制策略](#67-load-control-policy负载控制策略)

## 1. Agenda

### 1.1. Main Topics

1. 地址空间：逻辑地址、物理地址
2. 内存管理面临的问题和功能需求
3. 基本内存管理： 分区式、页式、段式、段页式
4. 基本内存管理的联系和区别
5. 基本内存管理的地址结构、地址转换和访存流程
6. 存储器的层次结构
7. 虚拟内存要解决的问题和功能需求
8. 虚拟内存管理和基本内存管理的区别
9. 虚拟内存的置换算法及其评价

### 1.2. Keys for Programming

1. understand different segment in programming
1. learn from address translation
1. learn from replacement algorithm (using in application cache)

## 2. Address Space地址空间

1. `physical address`
1. `logical address`: also known as `virtual address`
1. 32bit system: up to 2<sup>32</sup>=4GB `RAM` and `virtual address` support
1. 64bit system
    1. in theory: up to 2<sup>64</sup> = 16EB Bytes
    1. actually: up to 2<sup>36</sup> = 64GB `RAM` and 2<sup>48</sup> = 256TB `virtual address` support

>However, the size of virtual memory each process can use is not determined by the amount of backing store a system has, but rather the architecture of the microprocessor the process is running on, and in part, the operating system kernel that performs the virtual memory management.
>
>That is, even though the x86_64 architecture supports a 64-bit virtual address space in theory, most x86_64 microprocessors only implement a 48-bit address pointer in practice. That is then further limited by the innards of the operating system (for example, of the 256TB the 48 bits give you, Windows before the 8.1 release only supported 8TB of user-space virtual memory per process).
>
>In 32-bit x86 systems on the other hand, the address pointer is 32 bits, which gives you a total of 4GB of directly addressable space, but since the processes need to communicate to the kernel as well, the 4GB was split into 3GB for user-space and 1GB for kernel-space data in most 32-bit operating systems.
>
>The important thing to take away here is that each process gets the full address space that a given architecture/operating system combination supports, regardless of how much memory there actually is in a system.
>
>1. Re: Virtual memory without swap space: <https://learn.redhat.com/t5/Platform-Linux/Virtual-memory-without-swap-space/td-p/2679>
>1. How many address lines are there in a typical Intel Core i5 machine?: <https://www.quora.com/How-many-address-lines-are-there-in-a-typical-Intel-Core-i5-machine>

## 3. Programming Phases编程的阶段

1. program
    1. coding
        + src && header
        + build conf: `configure`, `setting.xml`
    1. building: `./configure` && `make`
        1. pre-compile: `gcc -E hello.c -o hello.i`
        1. compile: `gcc -S hello.i -o hello.s`
        1. assemble: `gcc -c hello.s -o hello.o`
        1. link: `ld a.o b.o -e main -o ab`
            + static link
            + dynamic link stub
    1. install: `make install`
1. process
    1. loading: allocate memory && init process
    1. running
        + relocate && protect
        + running loading

![programming_phases](./.assets/diagram/programming_phases.svg)

> 程序的编译、链接、装载与运行: <https://www.nosuchfield.com/2018/11/23/Program-compilation-linking-loading-and-running/>

## 4. Simple Memory Management基本内存管理

### 4.1. Allocate Types分配类型

|-|固定分区|可变分区|分页|分段|
|-|:-:|:-:|:-:|:-:|
|固定大小|yes|no|yes|no|
|离散分布|no|no|以页离散|以段离散|
|element type|physical|physical|physical|logical|
|管理者|`OS`|`OS`|`OS`|`OS`+`programer`/`compiler`|
|programming transparency|yes|yes|yes|no|
|地址空间维数|一维|一维|一维|多维|
|空间浪费|内零头|外零头|内零头|N/A|

1. contiguous allocations连续分配
    + single contiguous partitioning单一连续分区
    + fixed partitioning固定分区
    + dynamic partitioning动态分区/可变分区
1. discrete allocations离散分配
    1. paging分页/页式
    1. segmentation分段/段式
    1. segmentation paging段页式
1. 分区、分页、分段的区别与联系
    + 演进角度： 分页是分区的演进，分段是基于分区的进程内部结构管理
    + 相融角度： 分区与分页互斥，分段是一种特殊的分区，分页与分段可相融形成段页式
    + 地址角度： 分区的进程虚拟地址是一个整体地址，分页和分段的进程虚拟地址划分成多个部分
1. 页面大小
    + 太大或太小均可能会导致缺页率上升，最佳页面大小与具体进程的访问相关
    + 页面小 = > 内部碎片少，页表大
    + 页面大 = > 内部碎片多，页表小
    + 商用计算机页面大小一般在0.5KB至64KB之间，最常见的是4KB或8KB，`x86`/`x64`为4K

### 4.2. Address Translation地址转换

![Segmented-Paging-Translating-Logical-Address-into-Physical-Address-Diagram](./.assets/image/Segmented-Paging-Translating-Logical-Address-into-Physical-Address-Diagram.png)

## 5. Memory Hierarchy存储器的层次结构

### 5.1. Memory Hierarchy in Type

1. for runtime运行时数据
    1. `register`寄存器
    1. `cache`高速缓存
    1. `main memory`主存
1. for storage持久化数据
    1. `secondary storage`外存
    1. `remote storage`远程存储/`network storage`网络存储

>为什么寄存器比内存快？: <http://www.ruanyifeng.com/blog/2013/10/register.html>
>
>1. 用料更好 = > `SRAM` vs `DRAM`
>1. 距离更短 = > light speed should consider in ns level, 1ns - - > 30cm, 3GHz - - > 0.3ns
>1. 关卡更少
>    + `ALU/CU` < - - > `register`
>    + `CPU` < - - > `FSB` < - - > `MC` < - - > `main memory`或`CPU` < - - > `iMC` < - - > `FSB` < - - > `main memory`

### 5.2. Memory Hierarchy in Real World

#### 5.2.1. Memory Performance in a Nutshell

![size_latency_and_bandwidth_of_memory_subsystem_components](./.assets/image/size_latency_and_bandwidth_of_memory_subsystem_components.jpg)

> Memory Performance in a Nutshell: <https://software.intel.com/content/www/us/en/develop/articles/memory-performance-in-a-nutshell.html>

#### 5.2.2. Computer Time in Human Terms

![computer_time_in_human_terms](./.assets/image/computer_time_in_human_terms.jpg)

>1. Compute Performance - Distance of Data as a Measure of Latency: <https://www.formulusblack.com/blog/compute-performance-distance-of-data-as-a-measure-of-latency/>
>1. Computer Latency at a Human Scale: <https://www.prowesscorp.com/computer-latency-at-a-human-scale/>

#### 5.2.3. Latency Comparison Numbers (~2012)

|Action|in ns|in us|in ms|Remark|
|--|--|--|--|--|
|L1 cache reference|0.5 ns|--|--|--|
|Branch mis-predict|5 ns|--|--|--|
|L2 cache reference|7 ns|--|--|14x L1 cache|
|Mutex lock/unlock|25 ns|--|--|--|
|Main memory reference|100 ns|--|--|20x L2 cache, 200x L1 cache|
|Compress 1K bytes with Zippy|3,000 ns|3 us|--|--|
|Send 1K bytes over 1 Gbps network|10,000 ns|10 us|--|--|
|Read 4K randomly from SSD*|150,000 ns|150 us|--|~1GB/sec SSD|
|Read 1 MB sequentially from memory|250,000 ns|250 us|--|--|
|Round trip within same data-center|500,000 ns|500 us|--|--|
|Read 1 MB sequentially from SSD*|1,000,000 ns|1,000 us|1 ms|~1GB/sec SSD, 4X memory|
|Disk seek|10,000,000 ns|10,000 us|10 ms|20x data-center roundtrip|
|Read 1 MB sequentially from disk|20,000,000 ns|20,000 us|20 ms|80x memory, 20X SSD|
|Send packet CA->Netherlands->CA|150,000,000 ns|150,000 us|150 ms|--|

$1 ns = 10^{-9} seconds$
$1 us = 10^{-6} seconds = 1,000 ns$
$1 ms = 10^{-3} seconds = 1,000 us = 1,000,000 ns$

>Latency Numbers Every Programmer Should Know: <https://gist.github.com/jboner/2841832>

## 6. Virtual Memory虚拟内存

### 6.1. VM in SR Perspective

1. the problems:
    1. huge task cannot running with a certain (small) main memory
    1. many tasks cannot running with a certain (small) main memory
    1. cannot afford large main memory
1. the business requirements/the goals:
    1. huge task can running with a certain (small) main memory
    1. many tasks can running with a certain (small) main memory
    1. each task should suffer just a little performance lack
    1. system should have better load balance
1. the user requirements:
    1. setting part of `local external storage` virtual to `main memory`
    1. use `virtual memory` transparent after setting
1. the function requirements:
    1. resident set management/policy/algorithm驻留集管理
    1. placement policy/management/algorithm放置策略
    1. fetch policy/management/algorithm调入策略
    1. replacement algorithm/policy/management置换算法
    1. cleaning policy/management/algorithm清理策略（post-replacement policy）
    1. load control policy/management/algorithm负载控制

### 6.2. Reconsider Medium-Term Scheduling and Swap再议"中程调度与交换"

1. **`Medium-Term Scheduling`**
    + also known as **`memory swapping`** or **`swapping scheduler`** in some articles
    + `suspend` process && swap out entire process
    + **the `swap space` in `secondary storage` is not `main memory` in logic交换区在逻辑上不属于内存空间**
1. **`Virtual Memory`**
    + also known as **`page replacement`**
    + doesn't need `suspend` process but swap some pages
    + **the `vm` in `secondary storage` is `main memory` in logic虚拟内存交换区在逻辑上属于内存空间（虚拟性）**
1. in contemporary `UNIX-like` still use `swap` name the `virtual memory` file or partition in `secondary storage`
    + `Linux`下运行`vmstat`查看`virtual memory`统计信息
    + `MacOSX`下运行`vm_stat`查看`virtual memory`统计信息

:question: is there segment swapping :question:

### 6.3. Resident Management驻留集管理

1. 分配页框数量
1. 分配策略/调整策略
    + 固定分配
    + 可变分配
1. 驻留集管理
    + 固定分配、局部置换
    + 可变分配、全局置换
    + 可变分配、局部置换

### 6.4. Fetch Policy调入策略

1. 何时调入： 可分别应用于首次调入和置换时，如：首次请调、置换预调
    + 请求调入
    + 预先调入
1. 何处调入
    + always from `swap`： `main memory` is the cache of `swap`
    + 修改页从`swap`调入，未修改页从`file system`调入： `main memory` is the cache of `swap` **when the `page` is modified**
    + swapping between `main memory` and `swap`： `swap` is the expansion of `main memory`

### 6.5. Replacement Algorithm置换算法

1. :star2: `OPT(Optimal)`最优置换算法
1. :star2:  `FIFO(First In First Out)`先进先出置换算法
1. :star2:  `LRU(Least Recently Used)`最久未使用置换算法：**replace based on access time** , replaces the page in memory that has not been referenced for the longest time
1. :star2:  `(simple) clock algorithm`时钟置换算法： `LRU`的一种近似算法
1. `NRU(Not Recently Used)`最近未使用置换算法： removes a page at random from the lowest-numbered nonempty class( **_Modern Operating System 4th Edition_ page 211**), `NRU`是`LRU`的一种近似算法，`NRU`是一种`clock algorithm`，比`(simple) clock algorithm`多一个`M`位表示"自调入内存后是否修改"
1. `LFU(Least Frequently Used)`最近最少使用置换算法： **replace based on access frequency**, replace the block in the set that has experienced the fewest references.

:exclamation:

不少教材或资料关于`LRU`和`LFU`存在以下问题：

1. `LRU`、`LFU`混淆不清，`LRU`基于最近一段时间内的上一次访问时间，`LFU`基于最近一段时间内的访问频次，是不同的两种算法
1. `LRU`翻译成"最近最少使用置换算法"，翻译成"最久未使用置换算法"（或"最近最久未使用置换算法"）更合适

:exclamation:

>1. The difference between LRU and LFU: <https://topic.alibabacloud.com/a/the-difference-between-lru-and-lfu_8_8_10265781.html>
>1. 缓存淘汰算法LRU和LFU： <https://www.jianshu.com/p/1f8e36285539>

### 6.6. Cleaning Policy/Post-Replacement Policy清理策略

1. on demand cleaning/instant cleaning
1. precleaning/batch cleaning

### 6.7. Load Control Policy负载控制策略

1. load control: determines the number of processes that will be resident in main memory
1. how to know when to increment or decrement process number
    + in `clock algorithm` measure pointer speed
1. which process to suspend
    + lowest priority process
    + faulting process
    + last process activated
    + process with smallest resident set
    + largest process
    + process with the largest remaining execution window
