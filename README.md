# 关于《操作系统原理》课程

- [1. 关于仓库](#1-关于仓库)
    - [1.1. 许可协议](#11-许可协议)
    - [1.2. 编写环境和使用方法](#12-编写环境和使用方法)
    - [1.3. 帮助完善](#13-帮助完善)
    - [1.4. 组织结构](#14-组织结构)
    - [1.5. `emoji`表达的含意](#15-emoji表达的含意)
- [2. 关于课程](#2-关于课程)
    - [2.1. 课程基本信息](#21-课程基本信息)
    - [2.2. 课程教材与实验教材](#22-课程教材与实验教材)
- [3. Futhermore](#3-futhermore)
    - [3.1. Standards and Specifications](#31-standards-and-specifications)
    - [3.2. Documents and Supports](#32-documents-and-supports)
    - [3.3. Manuals and CheatSheets](#33-manuals-and-cheatsheets)
    - [3.4. Books and Monographs](#34-books-and-monographs)
        - [3.4.1. Theory, Design and Implement理论、设计与实现](#341-theory-design-and-implement理论设计与实现)
        - [3.4.2. Application and Usage应用与使用](#342-application-and-usage应用与使用)
        - [3.4.3. Relative其他相关](#343-relative其他相关)
    - [3.5. Courses and Tutorials](#35-courses-and-tutorials)
    - [3.6. Papers and Articles](#36-papers-and-articles)
    - [3.7. Playgrounds and Exercises](#37-playgrounds-and-exercises)
    - [3.8. Examples and Templates](#38-examples-and-templates)
    - [3.9. Auxiliaries and Tools](#39-auxiliaries-and-tools)
    - [3.10. Miscellaneous](#310-miscellaneous)

## 1. 关于仓库

### 1.1. 许可协议

本仓库使用`CC-BY-SA-4.0`协议，更详细的协议内容请见<https://gitlab.com/arm_commons/commons/-/blob/master/LICENSES/CC-BY-SA-4.0/README.md>

### 1.2. 编写环境和使用方法

请详见<https://gitlab.com/arm_commons/commons/-/blob/master/ENVS/MD_RAW/README.md>

### 1.3. 帮助完善

期待您一起完善本仓库，您可以通过以下方式帮助完善本仓库：

1. **`merge request`**：通过`GitLab`的`merge request`到`master`分支
1. **`issue`**： 通过`GitLab`或`Gitee`的`issue`发起一个新的`issue`（标签设置成`optimize`）

本仓库地址：

1. 主地址： <https://gitlab.com/arm_courses/os>
1. 镜像地址： <https://gitee.com/aroming/course_os>

### 1.4. 组织结构

1. 目录组织
    1. **`Addons/`** ： 附加文档，如：专题类的论述
    1. **`Experiments/`** ： 实验指导文档
    1. **`Outlines/`:** ： 各部分的大纲
    1. **`Exercises`** ： 习题及其解析
    1. **`README.md`** ： 本文件
1. 章节对应
    1. **`Part_01`** 或 **`pt01`** ： Operating System Overview操作系统概述
    1. **`Part_02`** 或 **`pt02`** ： Process Management进程管理
    1. **`Part_03`** 或 **`pt03`** ： Memory Management内存管理
    1. **`Part_04`** 或 **`pt04`** ： Device Management设备管理
    1. **`Part_05`** 或 **`pt05`** ： File Management文件管理

### 1.5. `emoji`表达的含意

1. 📖 纸质出版物
1. 💻 电子出版物
1. 🌐 互联网资源
1. 👑 顶级好评（权威）
1. 👍 一级好评
1. 🌟 二级好评，或重要
1. ⭐ 好评，或次重要
1. 🆓 免费
1. 💰 收费

## 2. 关于课程

### 2.1. 课程基本信息

1. 课程名称： 《计算机操作系统/操作系统》（ _Computer Operating System_ ），专业基础课，计算机类硕士研究生入学考试初试专业课统考课程（四门课程之一）
1. 先修课程： 《计算机组成原理》（或包含组成原理内容的相关课程），《微机原理与接口技术》，《接口与通讯》，《C语言程序设计》，《数据结构》
1. 课程目标：
    1. **When && Who && Why** ： 了解OS的源由和演变历史
    1. **What** ： 掌握OS的重要概念和功能
    1. **How** ： 掌握OS的基本原理、经典算法与数据结构
1. Keywords： Technical Terms, Principles, Algorithms, Data Structures

### 2.2. 课程教材与实验教材


1. 📖 辅助教材：[_OSIDP(Operating Systems Internals and Design Principles 9th Global Edition)_](https://www.pearson.com/us/higher-education/program/Stallings-Operating-Systems-Internals-and-Design-Principles-9th-Edition/PGM1262980.html). [美]William Stallings. Pearson
    - 📖 [《操作系统-精髓与设计原理（第8版.全球版）》](https://item.jd.com/12507600.html). [美] William，Stallings（威廉.斯托林斯） 著，郑然，邵志远，谢美意 译. 人民邮电出版社. 2019-01-01
    - 📖 [《操作系统-精髓与设计原理（第8版）》](https://item.jd.com/12140626.html). [美] William，Stallings（威廉.斯托林斯） 著，陈向群，陈渝 等 译. 电子工业出版社. 2017-02-01

## 3. Futhermore

### 3.1. Standards and Specifications

<!-- {standards, specifications, conventions goes here} -->

### 3.2. Documents and Supports

<!-- {trusted or official documents goes here} -->

### 3.3. Manuals and CheatSheets

<!-- {manuals goes here} -->

### 3.4. Books and Monographs

<!-- {publishes goes here} -->

#### 3.4.1. Theory, Design and Implement理论、设计与实现

1. 📖 Andrew S. Tanenbaum/Herbert Bos. [_Modern Operating Systems(4th Edition)_[M]](https://book.douban.com/subject/25864553/).
    - 📖 [《现代操作系统（原书第4版）》](http://www.cmpbook.com/stackroom.php?id=43456)，[荷-美]Andrew S. Tanenbaum，Herbert Bos 著，陈向群 译， 机械工业出版社，[豆瓣链接](https://book.douban.com/subject/27096665/)
2. 📖 [_Operating System Concepts(9th Edition)_](https://book.douban.com/subject/10076960/). Abraham Silberschatz / Peter B. Galvin / Greg Gagne. Wiley. ISBN: 9781118063330. 2012-12-17.
    - 📖 [《操作系统概念（原书第9版）》](https://item.jd.com/12395269.html). [美] 亚伯拉罕·西尔伯沙茨（Abraham Silberschatz） 彼得 B. 高尔文（Pet 著，郑扣根 译. 机械工业出版社. 2018-07-01
3. 📖 《操作系统设计与实现（原书第3版）》（[上册](https://www.phei.com.cn/module/goods/wssd_content.jsp?bookid=43010)，下册），[荷-美] Andrew S. Tanenbaum），[美] Albert S. Woodhull 著，陈渝，谌卫军 译，电子工业出版社，[豆瓣链接 -- 上册](https://book.douban.com/subject/2044818/)，[豆瓣链接 -- 下册](https://book.douban.com/subject/2044819/)
4. 📖 [_The Design and Implementation of the FreeBSD Operating System(2nd Edition)_](https://item.jd.com/11892504.html).[美]Marshall Kirk McKusick / [美]George V.Neville-Neil / [美]Robert N.M.Watson.人民邮电出版社.2016-3-1. ISBN: 9787115413499 . [豆瓣链接](https://book.douban.com/subject/26765606/)
5. 📖 💻 🆓 👍 Remzi H. Arpaci-Dusseau and Andrea C. Arpaci-Dusseau (University of Wisconsin-Madison). [_Operating Systems: Three Easy Pieces_[M]](https://pages.cs.wisc.edu/~remzi/OSTEP/).

#### 3.4.2. Application and Usage应用与使用

1. 📖 💻 [《鸟哥的Linux私房菜：基础学习篇（第4版）》](http://linux.vbird.org/linux_basic/)，[豆瓣链接](https://book.douban.com/subject/30359954/)，[GitBook链接](https://legacy.gitbook.com/book/wizardforcel/vbird-linux-basic-4e/details)
2. 📖 💻 [《鸟哥的Linux私房菜：服务器架设篇（第3版）》](http://linux.vbird.org/linux_server/)，[豆瓣链接](https://book.douban.com/subject/10794788/)，[GitBook链接](https://legacy.gitbook.com/book/wizardforcel/vbird-linux-server-3e/details)

#### 3.4.3. Relative其他相关

1. 📖 [深入理解计算机系统（原书第3版）](https://item.jd.com/12006637.html)(美) 兰德尔 E.布莱恩特（Randal E.·Bryant） 著，龚奕利，贺莲 译. 机械工业出版社. ISBN: 9787111544937. 2016-12-01，[豆瓣链接](https://book.douban.com/subject/1230413/)，[CS: APP blog](http://csappbook.blogspot.com/)
2. 📖 💻 [《UNIX编程艺术》](https://www.phei.com.cn/module/goods/wssd_content.jsp?bookid=33367)，[美] Eric S.Raymond 著，姜宏，何源，蔡晓骏 译，电子工业出版社，[豆瓣链接](https://book.douban.com/subject/1467587/)，[英文在线阅读官网](http://www.catb.org/~esr/writings/taoup/html/)
3. 📖 💻 [《只是为了好玩 : Linux之父林纳斯自传》](https://github.com/limkokhole/just-for-fun-linus-torvalds)，[芬-美]Linus Torvalds，David Diamond，[豆瓣链接](https://book.douban.com/subject/25930025/)，[GitHub链接（英文EPUB，PDF）](https://github.com/limkokhole/just-for-fun-linus-torvalds)

### 3.5. Courses and Tutorials

<!-- {systematic courses and tutorials goes here} -->

1. 🌐 [中国大学MOOC：电子科技大学《计算机操作系统》](https://www.icourse163.org/learn/UESTC-1205790811?tid=1206950287#/learn/announce)
2. 🌐 [清华大学：操作系统2019春](http://os.cs.tsinghua.edu.cn/oscourse/OS2019spring)
    - [课程问答](https://xuyongjiande.gitbooks.io/os-qa/index.html)
    - [实验指导书](https://chyyuu.gitbooks.io/ucore_os_docs/content/)
    - [实验源码](https://github.com/chyyuu/ucore_os_lab)
3. 🌐 [中国大学MOOC：华中科技大学《操作系统原理》](https://www.icourse163.org/course/HUST-1003405007#/info)
4. 🌐 [Stony Brook University: CSE 306 Operating Systems](https://www3.cs.stonybrook.edu/~kifer/Courses/cse306/)
5. 🌐 [University of North Florida: COP 4610 Operating Systems](https://www.unf.edu/public/cop4610/)
6. 🌐 [南京大学 "操作系统：设计与实现" (蒋炎岩)](https://open.163.com/newview/movie/courseintro?newurl=DH4L0C9LJ)
    1. 🌐 [2020 南京大学计算机系统基础习题课 (蒋炎岩)](https://www.bilibili.com/video/BV1qa4y1j7xk?p=1&vd_source=a1dcf8b09c8775c535909f215fa774a3)

### 3.6. Papers and Articles

<!-- {special topics goes here} -->

1. 🌐 [Yanyan's Wiki（蒋炎岩）.](https://jyywiki.cn/)
2. 🌐 [Mac OS X 背后的故事.](https://bigeast.github.io/Mac_OS_X_%E8%83%8C%E5%90%8E%E7%9A%84%E6%95%85%E4%BA%8B.html)
3. 🌐 [易百教程：操作系统.](https://www.yiibai.com/os)
4. 🌐 [为什么计算机的学生要学习Linux开源技术.](http://tinylab.org/why-computer-students-learn-linux-open-source-technologies/)

### 3.7. Playgrounds and Exercises

<!-- {exercises here} -->

### 3.8. Examples and Templates

<!-- {examples here} -->

### 3.9. Auxiliaries and Tools

<!-- {auxiliaries here} -->

1. [Play with Docker.](https://labs.play-with-docker.com/)
2. 🌐 [`Linux Lab`](http://tinylab.org/linux-lab/)
    1. [GitHub repo](https://github.com/tinyclub/linux-lab.git)
    2. [Gitee repo](https://gitee.com/tinylab/linux-lab.git)

### 3.10. Miscellaneous

<!-- {misc here} -->
